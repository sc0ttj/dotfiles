# BASH aliases

# a better `. ~/.bashrc`
alias reload='exec $SHELL -l;'

# auto "fix" my common typos
alias alis='alias'
alias alais='alias'
alias aslias='alias'
alias aslisa='alias'
alias eco='echo'
alias ehco='echo'
alias ecoh='echo'
alias wihch='which'
alias wihhc='which'
alias whcih='which'
alias whcihc='which'
alias whcih='which'
alias wdhihc='which'
alias grpe='grep'
alias gerp='grep'
alias gfrep='grep'
alias ffepr='grep'
alias frep='grep'
alias ferp='grep'
alias histry='history'
alias histroy='history'
alias histpory='history'
alias histrpoy='history'
alias histroyt='history'
alias hisgroy='history'
alias hostry='history'
alias hostory='history'
alias hostroy='history'
alias hodstory='history'
alias hidstory='history'
alias hidstroy='history'
alias dhowdoi='howdoi'
alias howdi='howdoi'
alias howdo='howdoi'
alias howi='howdoi'

# bat (git aware, cat replacement with syntax highlighting)
if [ "$(which bat)" != "" ];then
  alias cat='~/bin/bat'
  alias bat='~/bin/bat --style numbers,changes'
fi

# bc (calculator)
alias bc='bc -l'
alias calculator='bc'

# cd
alias cd..='cd ..'
alias gh='cd ~'
alias gH='cd /mnt/home'
alias gD='cd ~/Downloads'
alias gDt='cd ~/Desktop'

# chmod
alias 000='chmod 000'
alias 644='chmod 644'
alias 755='chmod 755'
alias +x='chmod +x'
alias +r='chmod +r'
alias +w='chmod +w'

# clear
alias clear='echo && clear'

# colorls
if command -v colorls &>/dev/null; then
  alias lc='colorls -A --sd --gs'
  alias llc='colorls -lA --sd --gs'
fi

# docker
alias killdocker='docker rm -f $(docker os -a -q) 2>/dev/null'

# df (disk info)
alias diskspace='du -S | sort -n -r | less'
alias disk='df -h | grep sd |\
    sed -e "s_/dev/sda[1-9]_\x1b[34m&\x1b[0m_" |\
    sed -e "s_/dev/sd[b-z][1-9]_\x1b[33m&\x1b[0m_" |\
    sed -e "s_[,0-9]*[MG]_\x1b[36m&\x1b[0m_" |\
    sed -e "s_[0-9]*%_\x1b[32m&\x1b[0m_" |\
    sed -e "s_9[0-9]%_\x1b[31m&\x1b[0m_" |\
    sed -e "s_/mnt/[-_A-Za-z0-9]*_\x1b[34;1m&\x1b[0m_"'

# fc-cache (fonts)
alias font_update='fc-cache -fv'
alias lsfont='fc-list : family'

# fd command (a faster find command)
alias fd='fd --hidden --ignore-file ~/.fdignore --max-depth 20 -j $(nproc --all)'

# free (memory info)
alias meminfo='free -m -l -t'

# git
alias gadd='git add'
alias gcommit='git commit -m'
alias gcheckout='git checkout'

# grep
alias grep='grep --color=auto --exclude-dir=.cvs --exclude-dir=.git --exclude-dir=.hg --exclude-dir=.svn'
alias egrep='grep -E --color=auto --exclude-dir=.cvs --exclude-dir=.git --exclude-dir=.hg --exclude-dir=.svn'
alias fgrep='grep -F --color=auto --exclude-dir=.cvs --exclude-dir=.git --exclude-dir=.hg --exclude-dir=.svn'

# history
alias hs='history | grep -i '
alias hgrep='history | grep -i'
alias topcommands='history | awk "{print $2}" | awk "{print $1}" |sort|uniq -c | sort -rn | head -10'

# ls
alias la='ls -a --color=auto'         # list hidden files too
alias ll='ls -l --color=auto'         # detailed view
alias lla='ls -la --color=auto'       # detailed view, show hidden file too
alias lls='ls -shaxSr --color=auto'   # sort by size
alias lspath='echo -e ${PATH//:/\\n}' # print each PATH entry on a new line
alias lsldpath='echo -e ${LD_LIBRARY_PATH//:/\\n}' # print each LD_LIBRARY_PATH entry on a new line

if [ "$(which exa)" != "" ];then
  alias la='exa --icons --git --group-directories-first -a --color=auto'    # list hidden files too
  alias ll='exa --icons --git --group-directories-first -l --color=auto'    # detailed view
  alias lla='exa --icons --git --group-directories-first -la --color=auto'  # detailed view, show hidden file too
  alias lls='exa --icons --git --group-directories-first -la --color=auto --sort size'  # detailed view, sort by size
fi

# mkdir
alias mkdir='mkdir -pv'

# mkisofs
alias mkisofs='mkisofs -iso-level 3 -J -L -r'

# more
more='less'

# nvm
alias nvm='PREFIX="" nvm'

# ps (processes)
alias psinfo='ps -faxc -U $UID -o pid,uid,gid,pcpu,pmem,stat,comm'

# pygmentize
if [ "$(which pygmentize)" != "" ];then
  pygmentize='pygmentize -f terminal256 -O style=monokai -g'
fi

# reset
alias reset='clear; reset'

# sudo
alias sudo='sudo '    # enable aliases to be sudo’ed

# telnet stuff
alias cavebbs='telnet cavebbs.homeip.net'
alias chess='telnet freechess.org 5000'
alias starwars='telnet towel.blinkenlights.nl'
alias sdf='telnet sdf.org'
alias worldmap='telnet mapscii.me'

# tree
alias tree="tree -I '.git|.cache|node_modules|bower_components' --dirsfirst" # exclude cache, git, node and bower dirs

# wget
alias download_site='wget --random-wait -r -p -e robots=off -U mozilla' # $1 must be a url
