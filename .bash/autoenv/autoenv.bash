# Autoenv - auto setup env vars & shell based on current dir
#============================================================
#
# if Autoenv is installed, we should source it
# here, or it would overwrite my 'cd' func.
# See https://github.com/kennethreitz/autoenv
#
#############################################################
#
# If a directory contains a .env file, it will automatically
# be executed  when you cd into it.
#
# Autoenv will walk up the directories until the mount point
# and execute all .env files beginning **at the top**.
#
# Disclaimer
#
# Autoenv overrides cd. If you already do this, invoke
# `autoenv_init` in your custom cd func after sourcing
# activate.sh.
#
# Autoenv can be disabled via unset cd if you experience I/O
# issues with certain file systems, particularly those that
# are FUSE-based (such as smbnetfs).
############################################################

if [ -f ~/.autoenv/activate.sh ];then

  # Overwrite Default Global Settings
  #
  # You can unset any of the variables below to get
  # the default settings back.
  ############################################################


  # Authorized env files.
  # Default is ~/.autoenv_authorize
  AUTOENV_AUTH_FILE=~/.autoenv_authorized


  # Filename of the env file to look for.
  # Default is .env
  AUTOENV_ENV_FILENAME=.env


  # Set this variable (to anything) to flip the order
  # of .env files executed, to lowest first.
  # Default is disabled (starts at /, works down to $PWD)
  unset AUTOENV_LOWER_FIRST


  # The file to execute when leaving a directory.
  # Default is env.leave
  AUTOENV_ENV_LEAVE_FILENAME=.env.leave


  # Enable auto execution of env.leave when leaving a dir
  # Default is unset (disabled), lets enable it
  AUTOENV_ENABLE_LEAVE=true


  # Set this variable to silently authorize the
  # initialization of new environments
  # Default is unset (disabled), lets enable it
  #unset AUTOENV_ASSUME_YES
  AUTOENV_ASSUME_YES=true

  source ~/.autoenv/activate.sh

fi

