# Cheat (https://github.com/chrisallenlane/cheat)
###########################################################

export CHEAT_EDITOR="$EDITOR"

#export DEFAULT_CHEAT_DIR='~/.cheat'

# You can additionally look for more cheats in other dirs

#export CHEATPATH='/path/to/my/cheats'
#export CHEATPATH="$CHEATPATH:/path/to/more/cheats"

export CHEATCOLORS=true

