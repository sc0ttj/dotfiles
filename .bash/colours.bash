# Colors

export CLICOLOR=YES

# set some easy to remember colours
# Usage:  echo -e "${color_name} some text ${nocolor}"
black="\033[38;5;0m"
brown="\033[38;5;130m"
cyan="\033[38;5;6m"
red="\033[38;5;160m"
orange="\033[38;5;214m"
purple="\033[38;5;165m"
blue="\033[38;5;27m"
green="\033[38;5;34m"
yellow="\033[38;5;3m"
gray="\033[38;5;245m"
white="\033[38;5;15m"
bright_white="\033[38;5;255m"
light_purple="\033[38;5;141m"
light_blue="\033[38;5;39m"
light_cyan="\033[38;5;14m"
light_red="\033[38;5;1m"
light_green="\033[38;5;112m"
light_yellow="\033[38;5;11m"
light_gray="\033[38;5;250m"
flashing_red="\033[5m]033[38;5;160m"
nocolor="\033[0m"

# monokai ls colours
export LS_COLORS='no=00;38;5;250:rs=0:di=01;38;5;198:ln=01;38;5;37:mh=00:pi=48;5;230;38;5;136;01:so=48;5;230;38;5;136;01:do=48;5;230;38;5;136;01:bd=48;5;230;38;5;244;01:cd=48;5;230;38;5;244;01:or=48;5;235;38;5;160:su=48;5;160;38;5;230:sg=48;5;136;38;5;230:ca=30;41:tw=48;5;64;38;5;230:ow=48;5;235;38;5;33:st=48;5;33;38;5;230:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.gz=01;31:ex=01;38;5;82:*.cmd=00;38;5;82:*.exe=00;38;5;82:*.com=00;38;5;82:*.btm=00;38;5;82:*.bat=00;38;5;82:*.jpg=00;38;5;37:*.jpeg=00;38;5;37:*.png=00;38;5;37:*.gif=00;38;5;37:*.bmp=00;38;5;37:*.xbm=00;38;5;37:*.xpm=00;38;5;37:*.tif=00;38;5;37:*.tiff=00;38;5;37:*.pdf=00;38;5;98:*.odf=00;38;5;98:*.doc=00;38;5;98:*.ppt=00;38;5;98:*.pptx=00;38;5;98:*.db=00;38;5;98:*.aac=00;38;5;208:*.au=00;38;5;208:*.flac=00;38;5;208:*.mid=00;38;5;208:*.midi=00;38;5;208:*.mka=00;38;5;208:*.mp3=00;38;5;208:*.mpc=00;38;5;208:*.ogg=00;38;5;208:*.ra=00;38;5;208:*.wav=00;38;5;208:*.m4a=00;38;5;208:*.axa=00;38;5;208:*.oga=00;38;5;208:*.spx=00;38;5;208:*.xspf=00;38;5;208:*.mov=01;38;5;208:*.mpg=01;38;5;208:*.mpeg=01;38;5;208:*.3gp=01;38;5;208:*.m2v=01;38;5;208:*.mkv=01;38;5;208:*.ogm=01;38;5;208:*.mp4=01;38;5;208:*.m4v=01;38;5;208:*.mp4v=01;38;5;208:*.vob=01;38;5;208:*.qt=01;38;5;208:*.nuv=01;38;5;208:*.wmv=01;38;5;208:*.asf=01;38;5;208:*.rm=01;38;5;208:*.rmvb=01;38;5;208:*.flc=01;38;5;208:*.avi=01;38;5;208:*.fli=01;38;5;208:*.flv=01;38;5;208:*.gl=01;38;5;208:*.m2ts=01;38;5;208:*.divx=01;38;5;208:*.log=00;38;5;240:*.bak=00;38;5;240:*.aux=00;38;5;240:*.bbl=00;38;5;240:*.blg=00;38;5;240:*~=00;38;5;240:*#=00;38;5;240:*.part=00;38;5;240:*.incomplete=00;38;5;240:*.swp=00;38;5;240:*.tmp=00;38;5;240:*.temp=00;38;5;240:*.o=00;38;5;240:*.pyc=00;38;5;240:*.class=00;38;5;240:*.cache=00;38;5;240:';

# enable color support of ls and grep
if [ -x "$(which dircolors)" ]; then
  # enable colours for ls and grep
  test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
  alias ls='ls --color=auto --group-directories-first'
  alias grep='grep --color=auto'
  alias fgrep='fgrep --color=auto'
  alias egrep='egrep --color=auto'
fi

export MAN_COLORS=true

# enable colours for man
if [ "$MAN_COLORS" = "true" -a -x "$(which man)" ]; then
	man(){
		LESS_TERMCAP_mb=$'\e[01;31m'
		LESS_TERMCAP_md=$'\e[01;31m'
		LESS_TERMCAP_me=$'\e[0m'
		LESS_TERMCAP_se=$'\e[0m'
		LESS_TERMCAP_so=$'\e[01;44;33m'
		LESS_TERMCAP_ue=$'\e[0m'
		LESS_TERMCAP_us=$'\e[01;32m'
		man "$@"
	}
fi

# enable colors by default for `diff`
if [ -x "$(which diff-so-fancy)" ];then
  function diff {
    /usr/bin/diff -u "$@" | diff-so-fancy
  }
  export -f diff
elif [ -x "$(which colordiff)" ];then
  alias diff='colordiff'
fi


