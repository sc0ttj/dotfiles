# This file is sourced by ~/.bash/main.bash
# and sets some TAB completion stuff

[ -z $PS1 ] && return

# enable BASH completions

# Use bash-completion, if available
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
    . /usr/share/bash-completion/bash_completion


# check /usr/share/
if [ -f /usr/share/bash_completion/bash_completion ];then
    source /usr/share/bash_completion/bash_completion
fi

if [ -d /usr/share/bash_completion/completions ];then
  for file in $(find /usr/share/bash_completion/completions -type f -iname "*")
  do
    source "$file"
  done
fi


# check /usr/local/share/
if [ -f /usr/local/share/bash_completion/bash_completion ]; then
    source /usr/local/share/bash_completion/bash_completion
fi

if [ -d /usr/local/share/bash_completion/completions ];then
  for file in $(find /usr/local/share/bash_completion/completions -type f -iname "*")
  do
    source "$file"
  done
fi


# check ~/.local/share/ ($XDG_DATA_HOME)
if [ -f ~/.local/share/bash_completion/bash_completion ];then
    source ~/.local/share/bash_completion/bash_completion
fi

if [ -d ~/.local/share/bash_completion/completions ];then
  for file in $(find ~/.local/share/bash_completion/completions -type f -iname "*")
  do
    source "$file"
  done
fi


# check /etc
if [ -f /etc/bash_completion ];then
    source /etc/bash_completion
fi

if [ -d /etc/bash_completion.d ];then
  for file in $(find /etc/bash_completion.d -type f -iname "*")
  do
    source "$file"
  done
fi


# git completions
if [ -f /usr/share/bash-completion/completions/git ];then
  source /usr/share/bash-completion/completions/git
elif [ -f /etc/bash_completion.d/git ];then
  source /etc/bash_completion.d/git
fi

# Enable tab completion for `g` by marking it as an alias for `git`
if type _git &> /dev/null ]; then
	complete -o default -o nospace -F _git g;
fi;

# Add tab completion for SSH hostnames to ssh, scp, sftp, lftp,
# using info from ~/.ssh/config, ignoring wildcards
if [ -e "$HOME/.ssh/config" ];then
  ssh_hostnames="$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2- | tr ' ' '\n')"
  complete -o "default" -o "nospace" -W "${ssh_hostnames}" scp sftp lftp ssh;
fi

# Add `killall` tab completion for common apps
complete -o "nospace" -W "wpa_supplicant cmake bmake make pkg petbuild \
buildpet wget curl kodi kodi.bin ctorrent geany mplayer vlc mpv transmission \
transmission-cli palemoon google-chrome firefox firefox-bin neofetch \
inxi gimp gtkdialog-splash cupsd smb-client" killall;

# only list dirs and set TAB complete to append '/' to dirs for the command below:
complete -S '/' -A directory mkdir
complete -S '/' -A directory dirname
