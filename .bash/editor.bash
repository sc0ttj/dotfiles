# EDITOR

# This file is sourced by ~/.bash/main.bash


# set $EDITOR, the console text editor
for editor in micro slap vim vi nano ed ex
do
  if [ "$(which $editor)" != "" ];then
    export EDITOR="${editor:-vi}"
    export VISUAL="${editor:-vi}"
    break
  fi
done

export GUI_EDITOR="$EDITOR"
export GIT_EDITOR="$EDITOR"

# set Vim rc file
[ -f ~/.vimrc ] && export VIMRC='~/.vimrc'

