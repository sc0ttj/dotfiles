# Feh

[ "$(which feh)" = "" ] && return

# search current dir and subfolders for .jpg,.jpeg & .png 
# files and write them to a file in /tmp/ (such as 
# /tmp/filelist-4Gf) the last 3 characters are random. 
# feh will then load the filelist and sort the files by 
# modified time, display filename, auto-zoom and 800x600 
# resolution
function fehl {
  # -d    draw (show) filename 
  # -S    sort by (we choose mtime) 
  # -g    geometry
  # -f    file list
  # -Z    auto-zoom
  randomfile="filelist-$(< /dev/urandom tr -dc A-Za-z0-9 |head -c 3).txt"

  find . -type f \( -iname "*.jpg*" -or -iname "*.jpeg*" -or -iname "*.png*" \) -printf "%p\n" \
    | grep -v /thumbs/ >>  /tmp/$randomfile

  feh -d -S mtime  -Z -g 800x600 -f /tmp/$randomfile &
}


# Set default optons:
# -Z    auto-zoom
# -x    borderless
# -F    fullscreen
# -B    background style (we choose black)
alias feh='feh -Z -x -F -B black'
