# Better cd: print dir contents on entering,
# and if $1 is file, cd into its parent dir.

# Supports Exa, Colorls, Nerd Fonts (icons in terminal) and
# Autoenv (from kennethreitz), if they're installed.

# Has its own minimalist Autoenv implementation
# as a backup if Autoenv not installed.

# If ls after cd is enabled, cd will allow exa, colorls or
# regular ls options, and you can pass them straight to 'cd'.

# Colorls requires Ruby, and Nerd Fonts (installed in ~/.fonts).

# Choose the 'DejaVuSansMono' nerd font in ~/.fonts as your
# default terminal font. See '/root/dotfiles/.fonts/README.md'

# If not run interactively (and instead called by a script),
# it will fall back to regular 'cd' and disable 'ls', while
# still supporting dir-based environments :)
#
# Usage:
#
#   cd <dir> [opts]
#
# Example:
#
#   cd ~/Downloads -l
#
# See 'cd -h' for more info.

# if disabled in ~/.bash/settings, leave now
[ "$BASHRC_BETTERCD:-false" = false ] && return

# reset and backup the built in 'cd' command
unset cd
unalias cd 2>/dev/null

function cd {

  # if not being called by user (and being called by a script
  # or program) then use builtin in cd and dont print ls
  [[ $- != *i* ]] && builtin cd "$@" && return
  [ -z "$PS1"   ] && builtin cd "$@" && return


  # use the built in commands by default
  local bettercd='builtin cd'
  local betterls="$basicls"

  # support passing options to this func
  local opts="$2 $3 $4 $5 $6 $7 $8 $9"

  if [ -n "$DISPLAY" ];then
    # we have a DISPLAY, so use the Nerd Font powered ls
    # alternatives.. or just fallback to regular ls if needed
    if [ "$BASHRC_BETTERCD_LSCMD" != '' ];then
      # use cmd from ~/.bash/settings
      betterls="$BASHRC_BETTERCD_LSCMD"

    else
      # set some nicer default options for exa, color ls and regular ls
      local exa="exa --group-directories-first --git --git-ignore --icons"
      local colorls="colorls --gs --sd"
      local basicls="$(which ls) --color=always --group-directories-first"
      # then see what is installed, and use the best one available
      for lscmd in exa colorls /bin/ls /usr/bin/ls ls
      do
        if [ "$(which $lscmd)" != "" ];then
          betterls="$lscmd"
          # if using regular ls, set some nice defaults
          if [ "$betterls" = "ls" ];then
            betterls="$basicls"
          # if using exa, set some nice defaults
          elif [ "$betterls" = "exa" ];then
            betterls="$exa"
          # if using colorls, set some nice defaults
          elif [ "$betterls" = "colorls" ];then
            betterls="$colorls"
          fi
          break
        fi
      done
    fi
  fi

  # Support Autoenv: use the autoenv_cd command,
  # if that is available, in order to support
  # sourcing .env and .env.leave files on each cd
  if [ "$BASHRC_BETTERCD_AUTOENV" = true ] && \
     [ "$(type -t autoenv_cd)" = "function" ]
  then
    bettercd='autoenv_cd'
  fi

  # Here we use `cd && ls`, except our 'cd'
  # supports .env files, and our 'ls' supports
  # Nerd fonts icons and Git (if these are available)

  # if $1 was given and $bettercd is set
  if [ -n "$1" ] && [ "$bettercd" != "" ] ;then

    # if $1 is a dir, mimic cd, then ls
    if [ -d "$1" ];then
        ${bettercd} "$1" && ${betterls:-ls} $opts

    # if $1 is $PWD, we dont need to cd anywhere, we can
    # just print the dir contents and exit
    elif [ "$1" = "$PWD" ] || [ "$1" = '.' ];then
      ${betterls:-ls} ${opts}

    # if $1 is as dash, mimic cd, then ls
    elif [ "$1" = "-" ];then
        ${bettercd} "-" 1>/dev/null && ${betterls:-ls} $opts

    # if $1 is --, mimic cd, then ls
    elif [ "$1" = "--" ];then
        ${bettercd} "$2" && ${betterls:-ls} $(echo "$opts" | cut -f2-9)

    # if $1 is a file, cd to $(dirname $1), then ls
    elif [ -f "$1" ];then
        ${bettercd} "$(dirname "$1")" && ${betterls:-ls} $opts

    # also enable a `cd -h` or `cd --help`, which will print
    # (color)ls usage, minus the options already enabled above,
    # plus some bettercd specific stuff at the end
    elif [ "$1" = '-h' ] || [ "$1" = '-help' ] || [ "$1" = '--help' ];then

      echo "Usage: cd <DIR> [OPTS]"
      echo
      echo "A more powerful cd command, which supports Autoenv, file and dir icons, "
      echo "and Git status icons (requires colorls). Disable with  'unset cd', and "
      echo "re-enable with 'source ~/.bash/func.cd.bash'"
      echo

      # print the relevant help options, depending on whether
      # we are using colorls, regular ls or just regular cd
      if [ "$betterls" = "$colorls" ];then
        colorls -h 2>&1 | grep -vE '\-\-gs|\-\-sd|directories first' \
          | grep -v '^Usage' \
          | sed -e 's/colorls/cd <dir>/' \
          && echo "  * to view CURRENT DIR info faster, you can omit <dir>, and just pass the options:" \
          && echo '' \
          && echo "    cd -l" \
          && echo "    cd -dla" \
          && echo "    cd --tree --dirs" \
          && echo "    cd -h" \
          && echo '' \
          && echo "directory-based environments:" \
          && echo "" \
          && echo "    Put a .env file in a directory to automatically source it when " \
          && echo "    you cd into that dir. Also, .env.leave files are sourced when " \
          && echo "    you leave a directory (cd into another)."

      elif [ "$betterls" = "$exa" ];then
        exa --help | grep -vE 'exa ' | sed -e 's/exa/cd/g'

      # else, if not using any ls (only buitlin cd),
      # just print the regular cd help info and leave
      elif [ "$betterls" = "" ];then
        builtin cd --help

      # else, we are using regular ls, so print its help info
      else
        ls --help | grep -vE 'ls ' | sed -e 's/ls /cd /g' \
          && echo \
          && echo " To view CURRENT DIR info faster, you can omit <dir>, and just pass the options:" \
          && echo '' \
          && echo "  cd -lS" \
          && echo "  cd -1" \
          && echo "  cd --group-directories-first" \
          && echo "  cd -h"
      fi
      # help stuff printed, so now leave
      return

    # if first option starts with a dash
    elif [ "$(echo $1 | grep '^\-')" != "" ];then
      # assume it's a (color)ls option being passed
      # straight to cd, like:  cd -l
      # and in this case, simply ls the current dir
      if [ "$betterls" != "" ];then
        $betterls "$@"
      # else, ls is not being used, so pass options
      # straight to cd
      else
        builtin cd "$@" 1>/dev/null
      fi

    # if still nothing, AND $1 is not a dir
    elif [ ! -d "$1" ];then
        # last hope, maybe cd can find the dir given using CDPATH
        if [ "$CDPATH" != "" ];then
          builtin cd "$@" 1>/dev/null && ${betterls:-ls} $opts
        else
          echo "$1 is not a directory." >&2
          return 1
        fi
    fi

  else # no params given, mimic cd (go to ~), then ls
    ${bettercd} ~
    [ $? -eq 0 ] && [ "${betterls}" != "" ] && ${betterls:-ls}
  fi

  retval=$?

  # we changed dir, now source .env.leave..
  # Autoenv will have done so already, but if using our
  # built-in fall-back, we need to do it here

  # so, if autoenv enabled, but Autoenv not installed
  # @TODO - call only once at top of script -
  if [ "$BASHRC_BETTERCD_AUTOENV" = true ] && \
     [ "$(type -t autoenv_cd)" != "function" ]
  then

    # source '.env' in current dir...

    local dir="$PWD"
    # traverse down from / to $PWD to source all .env.leave(s)
    local count=$(echo "$PWD" | tr '/' '\n' | grep -v ^$| wc -l)
    count=$(($count + 1))
    local counter=1
    while [ $counter -le $count ]
    do
      dir="$(echo "${PWD}" | cut -f-"$counter" -d'/' | sed -e "s/^$/\//g")"
      dir="${dir:-/}"
      counter=$(($counter + 1));
      [ -d "$dir" ] && [ -f "${dir}/.env" ] && source "${dir}/.env"
    done

    # source '.envleave' in $OLDPWD
    local dir="$OLDPWD"
    case $PWD/ in
      $OLDPWD*) :;; # $PWD is inside $OLDPWD, dont source $OLDPWD/.env.leave (we havent left!)
      *)
        # traverse up from $OLDPWD, source all the '.env.leave' files up to $PWD
        while [ "$dir" != "/" ]
        do
          # we definitely haven't left $PWD, so skip sourcing $PWD/.env.leave
          [ "$dir" = "$PWD" ] && break
          if [ -d "$dir" ] && [ -f "${dir}/.env.leave" ];then
            source "${dir}/.env.leave"
          fi
          dir="${dir%/*}" # strip last dir
          dir="${dir:-/}" # make / the top dir
        done
       ;;
    esac
  fi

  return $retval
}

# now setup better TAB completion for our shiny new cd command...

# this function enables fuzzy completion of dir names :)
function fuzzy_dirname_complete (){
    local RESULTS='' DIRPATH='' BASENAME='' FILTER='' MATCH=''
    # if no completion text, list all dirs in $PWD
    if [ -z "$2" ] || [ "$2" = "cd" ];then
        RESULTS="$(echo */ |tr ' ' '\n'  | grep -v './')"
    else
        # we have text to complete, get the dir name we are completing
        DIRPATH="$(echo "$2" | sed 's|[^/]*$||')"
        BASENAME=$(echo "$2" | sed 's|.*/||')
        # create FILTER as '*s*t*r*i*n*g*'
        FILTER=$(echo "$BASENAME" | sed 's|.|\0.*|g')
        # get the file & dirs in the given dir
        LS_RESULTS="$(echo "$DIRPATH"*/ | tr ' ' '\n')"
        # try again with ./prefixed
        LS_RESULTS="$(echo "$DIRPATH"*/ | tr ' ' '\n' | sed 's#^./##')"

        # filter out the files, and keep only the dirs matching $2,or $FILTER
        RESULTS="$(echo "${LS_RESULTS}" \
          | while read ls_item
            do \
              [ -z "$MATCH" ] && [ -d "$ls_item" ] && MATCH="$(echo "$ls_item" | grep -i -- "^$FILTER\$" )"
              [ -z "$MATCH" ] && [ -d "$ls_item" ] && MATCH="$(echo "$ls_item" | grep -i -- "^$FILTER"   )"
              [ -z "$MATCH" ] && [ -d "$ls_item" ] && MATCH="$(echo "$ls_item" | grep -i -- "$FILTER\$"  )"
              [ -z "$MATCH" ] && [ -d "$ls_item" ] && MATCH="$(echo "$ls_item" | grep -i -- "$FILTER"    )"

              [ ! -z "$MATCH" ] && echo "$MATCH" | sed 's#/$##'
            done)"
    fi

    # set the completion list in the array COMPREPLY, removing double slashes
    COMPREPLY=( $(echo ${RESULTS}) )
}

# now apply the completion function to our cd command..

#   -o nospace      dont append a space after typed text
#   -o dirnames     treat each item as a directory name
#   -S '/'          auto append a / to items in completion list
#   -F funcname     the function to use whic hbuilds the COMPREPLY array (the list of completion words)
#   -A directory    list only dirs in the completion options (ignored as we also use -F)
#   cd              the space separated list of the commands that use these completion settings (we're only setting 'cd' to use it here)
complete \
  -o nospace \
  -o dirnames \
  -S '/' \
  -F fuzzy_dirname_complete \
  -A directory \
  cd
