# Functions

# This file is sourced by ~/.bash/main.bash

# These functions should work in most Linux systems out of the box


# Note: functions defined here may not be usable from within
# programs like Ranger, w3m, vim, micro and so on, when starting
# sub-shells form within those programs.


# For functions/scripts that work anywhere (including when called from
# within ranger/w3m, etc) create a script in `~/bin/<script_name>`
# instead.


mkcd() {
  [ ! "$1" ] && echo "Usage:   mkcd 'path/to/dir'" && return 1
  mkdir -pv "$1" && cd "$1"
}

# Test whether file exists in current or parent directories
find_in_cwd_or_parent() {
  local slashes=${PWD//[^\/]/}; local directory=$PWD;
  for (( n=${#slashes}; n>0; --n )); do
    test -e "$directory/$1" && echo "$directory/$1" && return 0
    directory="$directory/.."
  done
  return 1
}

# Strip trailing whitespace, and ensure files end with a newline.
fix_whitespace() {
  local exclude_dirs;
  for d in .git tmp log public; do exclude_dirs+="-path './$d' -prune -o "; done
  eval find . $exclude_dirs -type f | \
  xargs file | grep -P "ASCII|UTF-8" | cut -d: -f1 | \
  xargs -I % sh -c "{ rm % && awk 1 > %; } < %; sed -i % -e 's/[[:space:]]*$//g'"
}

# Grep/Sed - Search and replace strings recursively in a given dir
gsed() {
  if [ -n "$2" ]; then
    if [ -n "$3" ]; then local path="$3"; else local path="."; fi
    ag -l "$1" "$path" | xargs -I {} -- sed -i "" -e "s%${1//\\/}%$2%g" {}
  else
    echo "== Usage: gsed search_string replace_string [path = .]"
  fi
}

# https://unix.stackexchange.com/questions/159253/decoding-url-encoding-percent-encoding
urlencode() {
    # urlencode <string>
    local length="${#1}"
    for (( i = 0; i < length; i++ )); do
        local c="${1:i:1}"
        case $c in
            [a-zA-Z0-9.~_-]) printf "$c" ;;
            *) printf '%%%02X' "'$c"
        esac
    done
}

# https://unix.stackexchange.com/questions/159253/decoding-url-encoding-percent-encoding
urldecode() {
    # urldecode <string>
    local url_encoded="${1//+/ }"
    printf '%b' "${url_encoded//%/\\x}"
}

# The functions below have corresponding key bindings in
# the ~/.bash/keybindings.bash file

# Git
function git_add {
	READLINE_LINE="git add "
	READLINE_POINT=8
}

function line_replacer {
	LINE="$1"
	LEN=${#LINE}
	READLINE_LINE="$LINE"
	READLINE_POINT=$LEN
}



# The func will print all history entries matching the currently typed command.
# It has corresponding key bindings in the ~/.bash/keybindings.bash file (Ctrl-h).
# Usage: type a command, hit Ctrl-h to see related history entries.
function view_history {
  local CURRENT_LINE=$READLINE_LINE
  local RESULTS=""
  local LINE_NO_LEAD_SPACE="$(echo -e "${CURRENT_LINE}" | sed -e 's/^[[:space:]]*//')"
  local red='\e[0;31m'
  local no_color='\e[0;0m'

  if [ "$LINE_NO_LEAD_SPACE" != "" ]; then
    echo -e "Searching history for ${LINE_NO_LEAD_SPACE}:"
    RESULTS=`history | grep "$LINE_NO_LEAD_SPACE"`
  fi

  if [ ! -z "$RESULTS" ]; then
    echo -e "$RESULTS"
    return 0
  fi

  echo -e "${red}No results found${no_color}"
  return 1
}

# clean up Python compiled and cache files
pyclean ()
{
  find . -name "__pycache__" -exec rm -r {} +
  find . -name "*.pyc" -exec rm {} +
}
