# FZF - fuzzy finder
#=========================================================================

mkdir ~/.config/fzf/ 2>/dev/null

# install and use  our custom key bindings from ~/.config/fzf
[ -f ~/.config/fzf/key-bindings.bash ] && cp ~/.config/fzf/key-bindings.bash ~/.fzf/shell/key-bindings.bash

# load main config file
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
[ -f ~/.config/fzf/fzf.bash ] && source ~/.config/fzf/fzf.bash

# exit now if fzf not installed
[ "$(which fzf)" = "" ] && return

# update the default FZF options:
  # --layout=reverse   put the search filter at the top
  # --height=95%       leave a space for console prompt and input line
  # --inline-info      put item count inline with search input
  # --border           put a border between search input and results
  # --ansi             allow colors
  # --no-hscroll       dont scroll horiztonally
  # -1                 automatically select the only match
  # -0                 exit immediately if no match
  # bind               Ctrl-o loads using 'rifle', Ctrl-y yanks to clipboard
export FZF_DEFAULT_OPTS="--layout=reverse \
  --height=95% \
  --inline-info \
  --border \
  --no-hscroll \
  -1 \
  -0 \
  --bind 'ctrl-o:execute(rifle -- {} < /dev/tty)+abort,ctrl-y:execute-silent(echo -n {} | xclip)+abort'"

if [ "$(which fd)" != "" ];then
  # Use fd as default fzf searcher (it respects .gitignore, faster than find)
  export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git --exclude .usr --exclude .cache --exclude buffers --exclude node_modules --ignore-file ~/.fdignore --max-depth 20'
  # To apply the command to the CTRL-T hotkey as well
  export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

  # Use fd (https://github.com/sharkdp/fd) instead of the default find
  # command for listing path candidates.
  # - The first argument to the function ($1) is the base path to start traversal
  # - See the source code (completion.bash) for the details.
  _fzf_compgen_path() {
    fd --hidden --follow --exclude ".git" --ignore-file ~/.fdignore . "$1"
  }

  # Use fd to generate the list for directory completion
  _fzf_compgen_dir() {
    fd --type d --hidden --follow --exclude ".git" --ignore-file ~/.fdignore . "$1"
  }
elif [ "$(which ag)" != "" ];then
  # Use fd as default fzf searcher (it respects .gitignore, unless 'find)
  export FZF_DEFAULT_COMMAND='ag -l --depth 10 --hidden -p ~/.agignore --silent --nobreak --nonumbers --noheading'
  # To apply the command to the CTRL-T hotkey as well
  export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
fi

# allow `cmd **[TAB]` to auto load fzf for the following commands:
# ag, micro, vi, tree, $EDITOR
# Run `complete | grep _fzf` to see the full list
complete -F _fzf_path_completion -o default -o bashdefault ag
complete -F _fzf_path_completion -o default -o bashdefault micro
complete -F _fzf_path_completion -o default -o bashdefault vi
complete -F _fzf_path_completion -o default -o bashdefault pkgdialog
complete -F _fzf_path_completion -o default -o bashdefault petget
complete -F _fzf_path_completion -o default -o bashdefault ${EDITOR:-vi}
complete -F _fzf_dir_completion -o default -o bashdefault tree


# Functions   (also see ~/.bash/keybindings.bash)
#=========================================================================

# fzcd [DIR] - cd to selected directory
function fzcd {
  local dir
  dir=$(find ${1:-.} -path '*/\.*' -prune \
      -o -type d -print 2> /dev/null | fzf +m) &&
  cd "$dir"
}


# fzf open - usage:  fzfopen [type to find the thing, ENTER to open/play it]
function fzfopen {
    local file

    file=$(fzf --ansi \
      --preview-window=right:70% \
      --query="$1" \
      --exit-0 \
      --preview='~/bin/bat --color always --style numbers,changes --wrap never {} \
         || pygmentize -f terminal256 -O style=monokai -g {} \
         || head -500 {} \
         || cat {}')

    [ -n "$file" ] || return 1

    case "$(file --mime-type -b "$file")" in
        text*)          	  ${EDITOR:-vim} "$file" ;;
        image*)        	    feh -Z -x -F -B black "$file" ;;
        audio*)      	      defaultaudioplayer"$file" ;;
        video*)      		    defaultmediaplayer "$file" ;;
        application/text*) 	${EDITOR:-vim} "$file" ;;
        application/xml) 	  ${EDITOR:-vim} "$file" ;;
        application/php) 	  ${EDITOR:-vim} "$file" ;;
        application/ruby) 	${EDITOR:-vim} "$file" ;;
        application/zip) 	  extract "$file" ;;
        application/bzip*)	extract "$file" ;;
        application/tar*)	  extract "$file" ;;
        application/gzip*)	extract "$file" ;;
        application/rar) 	  extract "$file" ;;
        *)               	  xdg-open "$file" ;;
    esac
}


# same as above but ALWAYS open file in a text editor ($EDITOR or vim)
function fzfedit {
    local file
    file=$(fzf --query="$1" --exit-0)
    [ -n "$file" ] || return 1
    ${EDITOR:-vim} "$file"
}


#fzf through cmd history, run the chosen cmd
function fzfhist {
    eval $(history | fzf +s | sed 's/ *[0-9]* *//')
}


#fzf through .desktop files, run the chosen one
function fzfrun {
    # note: xdg-open has a bug with .desktop files, so we cant use that
    selected="$(ls /usr/share/applications | fzf -1 -0)"
    `grep '^Exec' "/usr/share/applications/$selected" | tail -1 | sed 's/^Exec=//' | sed 's/%.//'` &>/dev/null &
}


# fzgrep [PATTERN] - fuzzy grep file contents
function fzgrep {
  local file
  file="$(grep -m1 -iRl "$@" | fzf -0 -1)"
  if [[ -f $file ]]
  then
     ${EDITOR:-vim} $file
  fi
}


# fzag [PATTERN] - fuzzy file contents search, requires ag (silversearcher)
function fzag {
  local file
  file="$(ag --nobreak --noheading $@ | fzf -0 -1 | cut -f1 -d':')"
  if [[ -f $file ]]
  then
     ${EDITOR:-vim} $file
  fi
}


# fzkill [PATTERN] - kill process
function fzkill {
  local pid
  pid=$(ps -ef | sed 1d | fzf -m | awk '{print $2}')

  if [ "x$pid" != "x" ]
  then
    echo $pid | xargs kill -${1:-9}
  fi
}


# Git specific fzf funcs for searching/filtering
# commit history
if [ "$(which git)" != "" ];then


  # fzgit-browse-history - Git log search, with diff previews :)
  function fzgit_browse_history {
    git log --graph --color=always \
        --format="%C(auto)%h%d %s %C(black)%C(bold)%cr" "$@" |
    fzf \
      --ansi \
      --preview-window=right:70%:wrap \
      --preview "echo {} | grep -o '[a-f0-9]\{7\}' | head -1 | xargs -I % sh -c 'git show --color=always %'" \
      --bind "enter:execute:
        (grep -o '[a-f0-9]\{7\}' | head -1 |
        xargs -I % sh -c 'git show --color=always % | less -R') << 'FZF-EOF'
        {}
FZF-EOF"
  }


  # helper aliases for fzfgit-checkout-commit
  alias glNoGraph='git log --color=always --format="%C(auto)%h%d %s %C(black)%C(bold)%cr% C(auto)%an" "$@"'
  alias _gitLogLineToHash="echo {} | grep -o '[a-f0-9]\{7\}' | head -1"
  alias _viewGitLogLine="$_gitLogLineToHash | xargs -I % sh -c 'git show --color=always % | diff-so-fancy'"

  # checkout git commit with previews
  function fzgit_checkout_commit {
    local commit
    commit=$( glNoGraph |
      fzf --no-sort --reverse --tiebreak=index --no-multi \
          --ansi --preview-window=right:70%:wrap \
          --preview="$_viewGitLogLine" ) &&
          git checkout $(echo "$commit" | sed "s/ .*//")
  }


  # checkout git branch/tag
  function fzgit_checkout_branch {
    local tags branches target
    tags=$(
      git tag | awk '{print "\x1b[31;1mtag\x1b[m\t" $1}') || return
    branches=$(
      git branch --all | grep -v HEAD             |
      sed "s/.* //"    | sed "s#remotes/[^/]*/##" |
      sort -u          | awk '{print "\x1b[34;1mbranch\x1b[m\t" $1}') || return
    target=$(
      (echo "$tags"; echo "$branches") |
      fzf-tmux -l30 -- --no-hscroll --ansi +m -d "\t" -n 2) || return
    git checkout $(echo "$target" | awk '{print $2}')
  }


  # easier way to deal with stashes
  # type `fzgit_stash` to get a list of your stashes
  #     - ENTER:   shows you the contents of the stash
  #     - Ctrl-d:  shows a diff of the stash against your current HEAD
  #     - Ctrl-b:  checks the stash out as a branch, for easier merging
  function fzgit_stash {
    local out q k sha
    while out=$(
      git stash list --pretty="%C(yellow)%h %>(14)%Cgreen%cr %C(blue)%gs" |
      fzf --ansi --no-sort --query="$q" --print-query \
          --expect=ctrl-d,ctrl-b);
    do
      mapfile -t out <<< "$out"
      q="${out[0]}"
      k="${out[1]}"
      sha="${out[-1]}"
      sha="${sha%% *}"
      [[ -z "$sha" ]] && continue
      if [[ "$k" == 'ctrl-d' ]]; then
        git diff $sha
      elif [[ "$k" == 'ctrl-b' ]]; then
        git stash branch "stash-$sha" $sha
        break;
      else
        git stash show -p $sha
      fi
    done
  }

fi



# Other functions

# Modified version where you can press
#   - CTRL-O:  open with `xdg-open` command,
#   - CTRL-E:  open with the $EDITOR
#   - ENTER:   open with the $EDITOR
function fo {
  local out file key
  IFS=$'\n' out=($(fzf-tmux --query="$1" --exit-0 --expect=ctrl-o,ctrl-e))
  key=$(head -1 <<< "$out")
  file=$(head -2 <<< "$out" | tail -1)
  if [ -n "$file" ]; then
    [ "$key" = ctrl-o ] && xdg-open "$file" || ${EDITOR:-vim} "$file"
  fi
}

