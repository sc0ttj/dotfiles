# Git

alias gadd='git add'
alias gcommit='git commit -m'
alias gcheckout='git checkout'


# usage:   getrepourl 'user/repo',
# or:      getrepourl 'github.com/user/repo',
# returns 'https://github.com/user/repo.git'

# and:     getrepourl 'gitlab.com/user/repo',
# returns 'https://gitlab.com/user/repo.git'
#
# or can take 'github.com/user/repo'
# and 'gitlab.com/user/repo'
function getrepourl {
	local in="$1"
	local slashes="${in//[^\/]}"
	local slash_count="${#slashes}"

	user="$(echo "$1" | cut -f1 -d '/')"
	repo="$(echo "$1" | cut -f2 -d '/')"
	if [ "$slash_count" != "1" ];then
		site="$(echo "$in" | cut -f1 -d '/')"
		case "$site" in
			github.com) ;;
			gitlab.com) ;;
			*) echo "Sorry, $site not supported" && return 1 ;;
		esac

		user="$(echo "$in" | cut -f2 -d '/')"
		repo="$(echo "$in" | cut -f3 -d '/')"
	fi

	echo "https://${site:-github.com}/${user}/${repo}.git"
	return 0
}

# set your Git credentials
setgit {
  # The first function argument may be used to change the name
  git config --local --add user.name "${1:-noname}"
  # The second function argument may be used to change the email
  git config --local --add user.email "${2:-email@example.com}"
}

