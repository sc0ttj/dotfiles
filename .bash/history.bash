# History

export HISTFILESIZE=90000
export HISTSIZE=90000

# ignore common or sensitive commands in history
export HISTIGNORE="ls:ll:la:lla:pwd:gh:gs:gl:pwd:history:hs:clear"

# Type stuff, hit <up>, see all previous matching commands
bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'

HISTCONTROL=ignoredups    	# ignore duplicates
HISTCONTROL=ignorespace   	# ignore cmds with leading space
HISTCONTROL=ignoreboth    	# dont add successive cmds

