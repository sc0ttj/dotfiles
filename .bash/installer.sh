#!/bin/bash

# Dotfiles Installer
# WARNING, this will overwrite your dotfiles!

# Check requirements..
[ -x /usr/sbin/pkg ] && pkg_is_installed=true || pkg_is_installed=false

if [ "$(which gcc)" = "" ];then
  echo "You must have GCC installed."
  echo
  echo "In Ubuntu or Debian, install the 'build-essentials' package."
  echo "In Puppy Linux, load the devx SFS file."
  echo
  echo "Then re-run this script."
  exit 1
fi

if [ "$(which git)" = "" ];then
    echo "You must have Git installed."
    exit 1
fi

# if running a modern Puppy Linux, install Pkg:
# a puppy CLI package manager
if [ -f /etc/DISTRO_SPECS ];then
  if [ "$pkg_is_installed" != true ];then
      mkdir -pv ~/gitlab/Pkg
      cd ~/gitlab/Pkg
      git clone https://gitlab.com/sc0ttj/Pkg .
      ./installer.sh
      echo
  fi
fi

cd ~

# Copy all our files over to $HOME
rm -rf ~/.git/ 2>/dev/null
cp -RT --remove-destination ~/dotfiles/ ~/
rm -rf ~/.git/ 2>/dev/null

# get settings
source ~/.bash/settings

# make sure ~/bin is in $PATH
if [ "$(echo "$PATH" | grep '~/bin')" = "" ];then
  PATH="${PATH}:~/bin"
  export PATH
fi


# Install our dependencies

# install stuff using Pkg

echo "Installing dependencies: This will take a while... "

pkg welcome &>/dev/null
echo
pkg update-sources
echo
pkg repo stretch-main
echo

ranger_deps="atool_ w3m_ w3m-img_ mediainfo_ odt2txt_ ffmpegthumbnailer_ yank_ feh_ "
micro_deps="tidy_ perltidy_ "
exa_deps="libhttp-parser2.1 "
mlterm="mlterm_ mlterm-common_ "
python2="python-chardet_ python-setuptools_ python-pkg-resources_ python-pip_ python-pip-whl_ "
python3="python3_ python3.5_ python3.5-minimal_ libpython3-stdlib_ python3-pip_ python3-wheel_ "
#ruby="ruby_ ruby-dev_ ruby2.3_ ruby2.3-dev_ libruby2.3_ rake_ gem_ rubygems-integration_ "
ruby=''


pkg add ncurses-term_ $ruby $python2 $python3 $ranger_deps $micro_deps $mlterm \
  $exa_deps silversearcher-ag_ redshift_ rofi_ xsel_ fbi_ \
  mtp_detect go-mtpfs_ mtp-tools_ colordiff_ bsdmainutils_ libdirectfb-bin_ \
  asciinema_  shellcheck_ git-extras_

echo

# fix python3
ln -s /usr/bin/python3.5 /usr/bin/python3 2>/dev/null

# fix pip if needed
pip3 --version 1>/dev/null || rm -rf /usr/local/lib/python3.5/dist-packages 2>/dev/null

# install stuff using pip...

# install wheel
pip install wheel
pip install setuptools --upgrade

# yapf (python linter, styler)
pip install yapf
# mycli (easier sql management on console)
pip install mycli
# cheat
pip install cheat
# howdoi
pip install howdoi
# tldr
pip install tldr
# ranger
pip install ranger-fm
. ~/dotfiles/.bash/ranger/ranger.bash
# pygments syntax highlighter
pip install Pygments
# mps-youtube and youtube-dl
pip3 install --user youtube-dl
pip3 install --user mps-youtube


#echo
#gem install
#gem install colorls


echo
# micro
cd /usr/bin/
curl https://getmic.ro | bash

cd ~

# fd
echo
wget https://github.com/sharkdp/fd/releases/download/v7.1.0/fd_7.1.0_i386.deb
pkg install ./fd_7.1.0_i386.deb && rm ./fd_7.1.0_i386.deb

# install from git repos, github, gitlab
echo
mkdir -pv ~/github
cd ~/github

# fzf
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install --xdg --key-bindings --completion --no-update-rc --no-zsh --no-fish
# install and use  our custom key bindings from ~/.config/fzf
[ -f ~/.config/fzf/key-bindings.bash ] && cp ~/.config/fzf/key-bindings.bash ~/.fzf/shell/key-bindings.bash

# ranger-devicons
git clone https://github.com/alexanderjeurissen/ranger_devicons.git
cd ranger_devicons
make install
cd ..

# git-quick-stats
git clone https://github.com/arzzen/git-quick-stats.git
cd git-quick-stats
PREFIX=/usr make install
cd ..

# git extra-commands
git clone https://github.com/unixorn/git-extra-commands.git
cd git-extra-commands
ln -s ~/github/git-extra-commands/bin/* ~/bin/ 2>/dev/null
cd ..

# pdftoroff, hovacui (pdf reader)
git clone https://github.com/sgerwk/pdftoroff.git
cd pdftoroff
make && strip -s ./fbhovacui ./hovacui ./xhovacui ./pdfannot ./pdffit ./pdftoroff && make install
cd ..

# xlunch (my fork for puppy)
git clone https://gitlab.com/sc0ttj/xlunch.git
cd xlunch
chmod +x ~/bin/genentries
cp -v ~/bin/genentries extra/genentries
make
strip -s ./xlunch
make install

# xlunchr (custom xlunch wrapper)
chmod -x /usr/bin/genentries
mkdir -p /usr/share/xlunch/themes/classic/
mkdir -p /usr/share/xlunch/icons/base
mkdir -p /usr/share/xlunch/icons/categories/20x20
cp -v ~/.config/xlunch/classic/bg.png /usr/share/xlunch/themes/classic/bg.png
cp -RTv ~/.config/xlunch/classic/icons /usr/share/xlunch/themes/classic/icons/
cp -RTv ~/.config/xlunch/icons /usr/share/xlunch/icons/
cd ..

# bash completions (installs to /usr/share/bash_completion/completions,
# and ~/.bash/completions.bash will find them)
git clone https://github.com/scop/bash-completion.git
cd bash-completion
autoreconf -i
./configure --prefix=/usr
make &>/dev/null
make install &>/dev/null

# cht.sh client
curl https://cht.sh/:cht.sh > ~/bin/cht.sh
chmod +x ~/bin/cht.sh

echo

# nvm, then node and npm LTS

echo "Installing NodeJS and NPM.. might take a while.."

unset PREFIX
export NVM_DIR="$HOME/.nvm"
rm -rf "$NVM_DIR"
mkdir -p "$NVM_DIR"

if [ "$BASHRC_NODEJS_VERSION" = "" ];then
  BASHRC_NODEJS_VERSION='--lts'
fi

if [ -d "$NVM_DIR" ];then
  # if previous install failed, we need to remove these
  rm ~/.nvmrc &>/dev/null
  rm -rf "$NVM_DIR"/src/ &>/dev/null
  # download and install nvm
  wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
  [ -s "$NVM_DIR/nvm.sh" ] && source "$NVM_DIR/nvm.sh" &>/dev/null
  # setup NPM properly
  [ -z "$NODE_PATH" ] && export NODE_PATH='/usr/lib/node_modules'
  # add './node_modules' to $PATH if not there
  echo "$PATH" | grep -q '\./node_modules/\.bin' || export PATH="${PATH}:./node_modules/.bin"
  # finally lets make sure we can call NVM in the terminal
  alias nvm='PREFIX="" nvm'
  [ "$(npm -v &>/dev/null)" = "" ] && nvm install $BASHRC_NODEJS_VERSION
  # if NPM is not in $PATH, reload it via NVM
  [ "$(npm -v &>/dev/null)" = "" ] && nvm use $BASHRC_NODEJS_VERSION
    # remove all the source if Node was compiled
  rm -rf "$NVM_DIR"/src/ &>/dev/null
  rm -rf "$NVM_DIR"/.cache/src/
fi
PREFIX=/usr
export PREFIX

#install using NPM
# coffee-fmt (linter, used by micro)
npm i -g coffee-fmt 2>/dev/null
# prettier   (linter, used by micro)
npm i -g prettier 2>/dev/null

echo

# setup some stuff...

# diff-so-fancy (already installed in ~/bin)
git config --global core.pager "diff-so-fancy | less --tabs=4 -RFX"
git config --global color.ui true
git config --global color.diff-highlight.oldNormal    "red bold"
git config --global color.diff-highlight.oldHighlight "red bold 52"
git config --global color.diff-highlight.newNormal    "green bold"
git config --global color.diff-highlight.newHighlight "green bold 22"
git config --global color.diff.meta       "yellow"
git config --global color.diff.frag       "magenta bold"
git config --global color.diff.commit     "yellow bold"
git config --global color.diff.old        "red bold"
git config --global color.diff.new        "green bold"
git config --global color.diff.whitespace "red reverse"



# the home stretch...

# get rc file to append to
bash_settings_file=''
for file in ~/.bashrc ~/.bash_profile ~/.profile
do
  # if file exists, use it and break
  [ -f $file ] && bash_settings_file=$file && break
done


# Now add our custom settings to the main BASH rc file
if [ "$(cat $bash_settings_file 2>/dev/null | grep -m1 '~/.bash/main.bash' )" = '' ];then
  echo "" >> $bash_settings_file
  echo "# Add custom user settings" >> $bash_settings_file
  echo ". ~/.bash/main.bash"      >> $bash_settings_file
  echo "" >> $bash_settings_file
fi

echo 'Updating fonts (adding Nerd fonts)...'
fc-cache -fv &>/dev/null

echo '
Installer finished  :)

To load your new terminal settings, run this command and hit ENTER:

  source ~/.bashrc && reload

Dont forget to update your terminal settings to use the
"DejaVu Sans Nerd Font" font.
'

[ -f ~/.bashrc ] && source ~/.bashrc
source $bash_settings_file

exit 0
