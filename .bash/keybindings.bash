# Keybindings

# This file is sourced by ~/.bash/main.bash

# Anything in this file will overwrite existing
# key bindings in /etc/inputrc and ~/.inputrc



# Ctrl-p to copy current line to clipboard
# (paste with Shift-Insert, Middle click, or Ctrl-V)
bind '"\C-p": "\C-e\C-u xsel <<"EOF"\n\C-y\nEOF\n\C-y"'

# Ctrl-h to show all occurences in history for entered line
# Requires the 'view_history' func in ~/.main/functions.bash
bind -x '"\C-h": view_history'


#####################################################################
# FZF
# also see ~/.fzf/shell/key-bindings.bash)

# ALT-C - cd into the selected directory
bind '"\ec": " \C-e\C-ufzcd\e\C-e\er\C-m"'

# ALT-T - remapping of FZFs default CRTL-T (fuzzy select to sdout)
bind '"\et": " \C-t"'

# Ctrl-o - open a file, using feh, $EDITOR, extract(), mocp, mplayer, xdg-open
# .. relies on reported mime type of chosen file.. See ~/.bash/fzf/
bind '"\C-o": " \C-e\C-u\C-y\ey\C-ufzfopen\e\C-e\er\C-m"'

# Alt-i to load ~/bin/file manager
bind '"\ei": " \C-e\C-ufilemanager\e\C-e\er\C-m"'


######################################################################
# FZF with Git

# Hold ALT, press g,<then more keys, see below>

# hold ALT.. press g,b
# => Git checkout branch
bind '"\eg\eb": " \C-e\C-ufzgit_checkout_branch\e\C-e\er\C-m"'

# hold ALT.. press g,c
# => Git Checkout Commit
bind '"\eg\ec": " \C-e\C-ufzgit_checkout_commit\e\C-e\er\C-m"'

# hold ALT.. press g,h
# => browse Git commit History
bind '"\eg\eh": " \C-e\C-ufzgit_browse_history\e\C-e\er\C-m"'

# hold ALT.. press g,d
# => view current Git diff
bind '"\eg\ed": " \C-e\C-ugit diff\e\C-e\er\C-m"'

