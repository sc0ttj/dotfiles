# Debian Stretch config

# This file should be sourced by ~/.bashrc or ~/.bash_profile

# exit if not sourced
[[ $0 = "$BASH_SOURCE" ]] && exit 1

# leave if not bash
[ -z "$BASH_VERSION" ] && exit 1

# If not running interactively, don't do anything
[[ $- != *i* ]] && exit 1
# same, but to be safe
[ -z "$PS1" ] && exit 1


# Environment Variables
######################################################################

# get latest Xdefaults
[ -f ~/.Xdefaults ] && xrdb -merge ~/.Xdefaults ~/.Xresources 2>/dev/null

# input keystrokes
[ -z "$INPUTRC"  ] && export INPUTRC='~/.inputrc'

# set MANPATH for man pages
[ -z "$MANPATH"  ] && export MANPATH='/usr/share/man:/usr/local/share/man'
# dont clear the screen after quitting a manual page
export MANPAGER='less -X';

# update the $PATH, copied from /etc/profile.. but add /root/.local/bin and /root/bin
export PATH="$PATH:/root/.local/bin:/root/bin"
# update $LD_LIBRARY_PATH from /etc/profile... but add /root/.local/lib
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/root/.local/lib"
# update the cd path, so you can access dirs INSIDE the dirs listed below more easily
# note, never export CDPATH (see https://mywiki.wooledge.org/BashPitfalls)
# CDPATH=.:/mnt/home:/root/Downloads:/root/Documents:/root/bin:/root/src:/root

TERM=xterm
# if tput reports 256 colors, enable them
[ "$(which tput)" != "" -a "$(tput colors 2>/dev/null)" = "256" ] && TERM=xterm-256color

# set a fancy prompt (non-color, unless we know we "want" color)

#
# NOT USED YET
#
case "$TERM" in
 xterm-color|*-256color) color_prompt=yes;;
esac

# enable color support of ls and also add handy aliases

if [ -x /usr/bin/dircolors ]; then
 test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
fi


case "$TERM" in
  mlter*)
    TERM=mlterm-256color
    ;;
  xter*)
    if [ -e /usr/share/terminfo/m/mlterm-256color ]; then
      # mlterm prefers this, and wont work nicely using "xterm-256color",
      # and it doesn't seem to mess up any other terminals (lxterminal, rxvt)
      TERM=mlterm-256color
    elif [ -e /usr/share/terminfo/x/xterm-256color ]; then
      TERM=xterm-256color
    elif [ -e /usr/share/terminfo/x/xterm-color ]; then
      TERM=xterm-color
    else
      TERM=xterm
    fi
    ;;
  linux)
    [ -n "$FBTERM" ] && TERM=fbterm ;;
esac

export TERM

if [ -n "$DISPLAY" ]; then
  # we have a $DISPLAY
  export BROWSER=defaultbrowser
  # but sometimes we just wanna stay in the console,
  # so set a custom variable for a console only browser
  export TERM_BROWSER=w3m
else
  # console session only, no X, set some
  # no-X specific settings

  # choose a good console only browser that
  # works without X
  for brwsr in w3m lynx links2 elinks
  do
    if [ "$(which $brwsr)" != "" ];then
      export BROWSER=${brwsr}
      export TERM_BROWSER=${BROWSER}
      break
    fi
  done
  # if in console, we probably just want xterm
  export TERM=xterm
fi

# Make Python use UTF-8 encoding for output to stdin, stdout, and stderr.
export PYTHONIOENCODING='UTF-8';

# API Keys - this file must not be saved into github, etc!
[ -f ~/.api_keys ] && source ~/.api_keys

# re-instate the default behaviour of BASH 4 hook 'command_not_found_handle'
unset command_not_found_handle &>/dev/null
unalias command_not_found_handle &>/dev/null
#  and 'cd', 'ls'
unset cd &>/dev/null
unset cd &>/dev/null
unset grep &>/dev/null
unalias cd &>/dev/null
unalias ls &>/dev/null
unalias grep &>/dev/null



# Interactivity and input (also see ~/.inputrc and ~/.main/keybindings.bash)
######################################################################
shopt -s checkwinsize		          # update LINES and COLUMNS
shopt -s autocd                   # makes `cd dir1` same as `dir1`
shopt -s cdspell                  # auto correct spelling
shopt -s cdable_vars              # assume invalid dir must be var containg dir
shopt -s dirspell                 # auto correct spelling
shopt -s cmdhist                  # save multi-line commands in same history entry
shopt -s lithist                  # save multi-line commands with \n not ;
shopt -s histappend               # append instead of overwrite history file
shopt -s checkhash                # check hashtable for previous cd commands (faster?)
shopt -s no_empty_cmd_completion  # dont search $PATH for completions on an empty line
shopt -s extglob                  # extended pattern matching features
shopt -s nocaseglob               # case-insensitive globbing (used in pathname expansion)
shopt -s hostcomplete             # autocomplete hostnames if starting with '@'


# Get BASH custom user settings (env vars used by other functions)
######################################################################
[ -f ~/.bash/settings ] && . ~/.bash/settings

# Third-party tools - (fzf, npm, rofi, etc) include only if installed
#####################################################################
# Get each dir in ~/.bash/, then load each *.bash in each dir
for cmd in $(ls -A -d -1 ~/.bash/* | grep -v '\.bash$')
do
  if [ -f "${cmd}/$(basename ${cmd}).bash" ];then
    . "${cmd}/$(basename ${cmd}).bash" 2>/dev/null
  fi
done


# TAB completions
######################################################################
source ~/.bash/completions.bash


# Aliases
#####################################################################
source ~/.bash/aliases.bash


# Functions - function based on common/popular/built-in commands
#####################################################################
source ~/.bash/functions.bash


# Colors
######################################################################
source ~/.bash/colours.bash


# Editor
######################################################################
source ~/.bash/editor.bash


# Pager
######################################################################
source ~/.bash/pager.bash


# History
######################################################################
source ~/.bash/history.bash


# Key bindings - can overwrite any previous bindings here
#####################################################################
source ~/.bash/keybindings.bash


# Prompt
#####################################################################
source ~/.bash/prompt.bash


# Other stuff - that needs to go last
#####################################################################

# Custom 'cd' func, with support for ls on cd, Nerd fonts,
# Autoenv (and lightweight fallback), Git aware, and more
source ~/.bash/func.cd.bash
