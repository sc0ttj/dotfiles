# Micro

function micro {
  echo -ne "\e]2; Editor \a"
  $(which micro) "$@"
  echo -ne "\e]2; Terminal \a"
}

