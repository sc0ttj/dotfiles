# Mlterm

UTF-8 enabled terminal, with good support for many languages.
  
## GUI Settings

* Press `Ctrl+Left Button` to bring up the GUI settings menu.
* Press `Ctrl+Right Button` to bring up the mini menu (defined in ~/.mlterm/menu).

## Config Files

* For good example config files, see https://github.com/Ahed91/Mlterm-Color-Schames
* Also see http://sophie.zarb.org/rpms/b9c7cc799c1beacffbdc3e0c92edf805/files

`mlterm` loads configuration files of "main", "font", "aafont", "color",
"key", "termcap", and "xim" on start up.  Configuration files for one
user are to be located in "~/.mlterm/" directory, while location for
configuration files for all users depends on the compilation option.
Possible locations are "/etc/", "/etc/X11/",
"/usr/X11R6/lib/X11/mlterm/", and so on.

The names and the roles of configuration files are:

```
 main   Main configuration items which can be overrided by command line
        options.

 font   Configurations for ordinary X fonts.

 aafont Configurations for anti-alias Xft fonts.

 color  Designate concrete RGB values for color names.

 key    Key definitions for special features of mlterm.

 termcap
        Define string sequences to be inputed by pressing control keys.

 xim    Define preset locales for X Input Methods which are shown in the
        GUI configurator.  Of course you can input XIM names and locales
        for the GUI configurator which are not listed in this
        configuration file.

```

 The contents of these configuration files consist of lines of "key=value"
 format.  Lines beginning with "#" are ignored.


## Example config files:

### `~/.mlterm/aafont`

```
ISO10646_UCS4_1 = 14,DejaVuSansMono Nerd Font 14;15,DejaVuSansMono Nerd Font 14
ISO8859_1 = 14,DejaVuSansMono Nerd Font 14;15,DejaVuSansMono Nerd Font 14
DEFAULT = 14,DejaVuSansMono Nerd Font 14
```

### `~/.mlterm/vaafont`

```
ISO10646_UCS4_1 = 14,DejaVuSansMono Nerd Font 14;15,DejaVuSansMono Nerd Font 14
ISO8859_1 = 14,DejaVuSansMono Nerd Font 14;15,DejaVuSansMono Nerd Font 14
DEFAULT = 14,DejaVuSansMono Nerd Font 14
```

### `~/.mlterm/font`

```
# This file refers to fonts installed in /usr/share/fonts/
# or ~/.fonts/.. Refresh system fonts with 'fc-cache -f'

DEFAULT = devajusansmono nerd font-medium-iso8859-1;
ISO8859_1 = devajusansmono nerd font-medium-iso8859-1;
ISO8859_15 = devajusansmono nerd font-medium-iso8859-15;
ISO10646_UCS2_1 = devajusansmono nerd font-medium-iso10646-1;
ISO10646_UCS2_1_BOLD = devajusansmonobold nerd font-iso10646-1;

# or uncomment these:

#DEFAULT = monaco-medium-iso8859-1;
#ISO8859_1 = monaco-medium-iso8859-1;
#ISO8859_15 = monaco-medium-iso8859-15;
#ISO10646_UCS2_1 = monaco-medium-iso10646-1;
#ISO10646_UCS2_1_BOLD = monaco-iso10646-1;

# or uncomment these:

#DEFAULT = inconsolata-medium-iso8859-1;
#ISO8859_1 = inconsolata-medium-iso8859-1;
#ISO8859_15 = inconsolata-medium-iso8859-15;
#ISO10646_UCS2_1 = inconsolata-medium-iso10646-1;
#ISO10646_UCS2_1_BOLD = inconsolata-iso10646-1;
```

### `~/.mlterm/color`

```
black=#000000
red=#b84131
green=#7da900
yellow=#c4a500
blue=#62a3c4
magenta=#ba8acc
cyan=#207383
white=#a1a1a1
hl_black=#7a7a7a
hl_red=#d6837c
hl_green=#c4f137
hl_yellow=#fee14d
hl_blue=#8dcff0
hl_magenta=#f79aff
hl_cyan=#6ad9cf
hl_white=#f7f7f7
2 = #4e4e9a990605
4 = #201f4a4a8787
```

### ``~/.mlterm/main`

```
app_name = mlterm | xterm
auto_restart = true | false | <restart command>
bel_mode = none | sound | visual
bg_color = <color name> | <color rgb>
borderless = false
brightness = <any ASCII decimal digit>
# (xterm,menu1,menu2,menu3, or command string like "mlclient -e w3m")
button3_behavior = xterm
cols = <any ASCII decimal digit>
col_size_of_width_a = 1 | 2
contrast = <any ASCII decimal digit>
cursor_bg_color = <color name> | <color rgb>
cursor_fg_color = <color name> | <color rgb>
# -- Encoding --
# (ISO-8859-[1-11], ISO-8859-[13-16], TIS-620 (same as ISO-8859-11),
#  KOI8-[RUT], ISCII, VISCII, TCVN5712, GEORGIAN_PS, CP125[0-8], CP874,
#  EUC-JP, EUC-JISX0213, Shift_JIS, Shift_JISX0213, ISO-2022-JP[1-3],
#  EUC-KR, UHC, JOHAB, ISO-2022-KR, GB2312 (EUC-CN), GBK, GB18030,
#  ISO-2022-CN, HZ, EUC-TW, BIG5, BIG5HKSCS, UTF-8, AUTO)
encoding = <encoding name> | auto
fade_ratio = <any ASCII decimal digit>
fg_color = <color name> | <color rgb>
fontsize = <any ASCII decimal digit> | larger | smaller
gamma = <any ASCII decimal digit>
icon_name = mlterm
icon_path = <file path>
input_method = <input method> ":" <input method specific options> ...
# (assamese,bengali,gujarati,hindi,kannada,malayalam,oriya,punjabi,roman,tamil,telugu)
iscii_lang = malayalam
line_space = <any ASCII decimal digit>
locale = <locale name>
logging_vt_seq = false
logsize = <any ASCII decimal digit>
max_ptys = <number>
mod_meta_key = none | mod1 | mod2 | mod3 | mod4 | meta | alt | super | hyper
mod_meta_mode = none | esc | 8bit
pty_list = (<pty dev>:<active flag>;)*
pty_name = <pty dev> | <pty title>
pwd = <current working directory>
receive_string_via_ucs = true | false
rows = <any ASCII decimal digit>
sb_bg_color = <color name> | <color rgb>
sb_fg_color = <color name> | <color rgb>
screen_height_ratio = <any ASCII decimal digit>
screen_width_ratio = <any ASCII decimal digit>
scrollbar_mode = none | left | right
# (simple,sample,sample2,sample3,next,motif,athena,mozmodern)
scrollbar_view_name = simple
ssh_public_key = 
ssh_private_key = 
ssh_keepalive_interval = 0
ssh_x11_forwarding = false
allow_scp = false
startup_ptys = 1
startup_screens = 1
static_backscroll_mode = true | false
step_in_changing_font_size = 1
tabsize = <any ASCII decimal digit> | 8
termtype = xterm | mlterm | kterm
title = <pty title>
type_engine = xcore | xft
use_alt_buffer = true | false
use_ansi_colors = true | false
use_anti_alias = true | false
use_bidi = true | false
use_clipboard = true | false
use_combining = true | false
use_ctl = true | false
use_dynamic_comb = true | false
use_extended_scroll_shortcut = false
use_ind = true | false
use_login_shell = true | false
use_multi_column_char = true | false
use_transbg = true | false
use_variable_column_width = true | false
vertical_mode = none | cjk | mongol
wall_picture = <file path>
word_separators = " ,.:;/@"
logging_vt_seq = true | false
selected_text(:encoding) = <none>
logging_msg = true | false
```

### `~/.mlterm/key `

```
#### Default settings
# UNUSED=IM_HOTKEY
# Control+F1=OPEN_SCREEN
# Control+F2=OPEN_PTY
# Control+F3=NEXT_PTY
# Control+F4=PREV_PTY
# Shift+F1=HSPLIT_SCREEN
# Shift+F2=VSPLIT_SCREEN
# Shift+F3=NEXT_SCREEN
# Shift+F5=CLOSE_SCREEN
# Shift+F6=HEXPAND_SCREEN
# Shift+F7=VEXPAND_SCREEN
# Shift+Prior=PAGE_UP
# Shift+Up=SCROLL_UP
# Shift+Insert=INSERT_SELECTION
# UNUSED=SWITCH_OSC52
# UNUSED=EXIT_PROGRAM
# Control+Button1="menu:mlterm-menu"
# Control+Button3="menu:mlconfig"

#### Examples
# Control+Right=NEXT_PTY
# Control+Left=PREV_PTY
# Button7=NEXT_PTY
# Button6=PREV_PTY
# Control+F5="proto:mlclient --serv localhost -e screen -xR"
# Control+F6="w3m http://www.google.co.jp/\n"
# Control+F7="proto:font_size=12;encoding=utf8"
# Control+F8="proto:input_method=ibus"
# Control+F9="proto:aafont:DEFAULT=Kochi Gothic"
# Control+F10="proto:color:black=rgb:ff/00/00"
# Control+F11="proto:use_local_echo=true"
# Control+F12="proto:use_local_echo=false"
# Control+F13="proto:broadcast=switch"
# Control+F14="proto:ignore_broadcasted_chars=switch"
# Button3="exesel:mlclient -e w3m"
# Button3="exesel:\"/cygdrive/c/Program Files/Internet Explorer/iexplore.exe\""
# Button3="exesel:/cygdrive/c/Program\\ Files/Internet\\ Explorer/iexplore.exe"
# Button4="\x1bOA\x1bOA"
# Button5="\x1bOB\x1bOB"
```

### `~/.mlterm/menu`

```
"Bidi Option" {
    "normal"    "bidi_mode=normal"
    "left"     "bidi_mode=left"
    "right"     "bidi_mode=right"
}
"Font" {
	"Larger"	"fontsize=larger"
	"Smaller"	"fontsize=smaller"
	-
	"antialias on"	"use_anti_alias=true"
	"antialias off"	"use_anti_alias=false"
}
"Encoding" {
	"Auto"		"encoding=auto"
	"UTF-8"		"encoding=UTF-8"
	"ISO 8859" {
		"ISO-8859-1"	"encoding=ISO-8859-1"
		"ISO-8859-2"	"encoding=ISO-8859-2"
		"ISO-8859-3"	"encoding=ISO-8859-3"
		"ISO-8859-4"	"encoding=ISO-8859-4"
		"ISO-8859-5"	"encoding=ISO-8859-5"
		"ISO-8859-6"	"encoding=ISO-8859-6"
		"ISO-8859-7"	"encoding=ISO-8859-7"
		"ISO-8859-8"	"encoding=ISO-8859-8"
		"ISO-8859-9"	"encoding=ISO-8859-9"
		"ISO-8859-10"	"encoding=ISO-8859-10"
		"ISO-8859-11"	"encoding=ISO-8859-11"
		"ISO-8859-13"	"encoding=ISO-8859-13"
		"ISO-8859-14"	"encoding=ISO-8859-14"
		"ISO-8859-15"	"encoding=ISO-8859-15"
		"ISO-8859-16"	"encoding=ISO-8859-16"
	}
	"8bit" {
		"KOI8-R"	"encoding=KOI8-R"
		"KOI8-U"	"encoding=KOI8-U"
		"KOI8-T"	"encoding=KOI8-T"
		"GEORGIAN-PS"	"encoding=GEORGIAN-PS"
		"TCVN5712"	"encoding=TCVN5712"
		"VISCII"	"encoding=VISCII"
		"CP1251"	"encoding=CP1251"
		"CP1255"	"encoding=CP1255"
	}
	"Japanese" {
		"EUC-JP"	"encoding=EUC-JP"
		"EUC-JISX0213"	"encoding=EUC-JISX0213"
		"Shift_JIS"	"encoding=SHIFTJIS"
		"SJISX0213"	"encoding=SJISX0213"
		"ISO-2022-JP"	"encoding=ISO-2022-JP"
		"ISO-2022-JP2"	"encoding=ISO-2022-JP2"
		"ISO-2022-JP3"	"encoding=ISO-2022-JP3"
	}
	"Korean" {
		"EUC-KR"	"encoding=EUC-KR"
		"JOHAB"		"encoding=JOHAB"
		"UHC"		"encoding=UHC"
		"ISO-2022-KR"	"encoding=ISO-2022-KR"
	}
	"traditional Chinese" {
		"Big5"		"encoding=BIG5"
		"Big5HKSCS"	"encoding=BIG5HKSCS"
		"EUC-TW"	"encoding=EUC-TW"
	}
	"simplified Chinese" {
		"EUC-CN (GB2312)" "encoding=EUC-CN"
		"GBK"		"encoding=GBK"
		"GB18030"	"encoding=GB18030"
		"HZ"		"encoding=HZ"
		"ISO-2022-CN"	"encoding=ISO-2022-CN"
	}
}
"XIM" {
	"kinput2"	"xim=kinput2:ja_JP.eucJP"
	"ami"		"xim=Ami:ko_KR.eucKR"
	"xcin (big5)"	"xim=xcin:zh_TW.Big5"
	"xcin (gb)"	"xim=zh_CN.GB2312:zh_CN.GB2312"
	"skkinput"	"xim=skkinput:ja_JP.eucJP"
}
"Full Reset"		"full_reset"
-
pty_list
"New PTY"	"open_pty"
```

### `~/.mlterm/termcap`

```
mlterm:\
	kD=\E[3~:kb=^?
xterm|xterm-256color|rxvt|rxvt-unicode:\
	kD=^?:kb=^H:kh=\E[H:@7=\E[F:ut
*:\
	kD=^?:kb=^H:kh=\E[H:@7=\E[F
```

### an alternative ~/.mlterm/termcap: 

```
mlterm:\
	k1=\E[11~:k2=\E[12~:k3=\E[13~:k4=\E[14~
xterm:\
	ut
rxvt:\
	kh=\E[7~:@7=\E[8~:k1=\E[11~:k2=\E[12~:k3=\E[13~:k4=\E[14~:ut
kterm:\
	kb=^H:kD=^?:k1=\E[11~:k2=\E[12~:k3=\E[13~:k4=\E[14~
```
