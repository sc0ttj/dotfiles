# Mplayer

# Set a low spec alias - to play videos in a lower 
# quality (use if video is stuttering, stalling, etc)
alias mplayer_lowspec='mplayer -framedrop -vfm ffmpeg -lavdopts lowres=1:fast:skiploopfilter=all'

