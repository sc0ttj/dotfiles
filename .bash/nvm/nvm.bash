# NVM, NodeJS and NPM
#####################################################################
# NVM is not compatible with PREFIX=/usr, so PREFIX
# needs to be unset before calling NVM.
unset PREFIX
export NVM_DIR="$HOME/.nvm"

. ~/.bash.settings

if [ "$BASHRC_NODEJS_VERSION" = "" ];then
  BASHRC_NODEJS_VERSION='--lts'
fi

if [ -d "$NVM_DIR" ];then
  rm -rf ~/.nvm/src/ &>/dev/null
  # load nvm
  if [ -s "$NVM_DIR/nvm.sh" ] && [ "$(which nvm)" = '' ];then
    source "$NVM_DIR/nvm.sh" &>/dev/null
  fi
  # load bash completion
  [ -s "$NVM_DIR/bash_completion" ] && source "$NVM_DIR/bash_completion"
  export PREFIX=/usr
  # setup NPM properly
  [ -z "$NODE_PATH" ] && export NODE_PATH='/usr/lib/node_modules'
  # add './node_modules' to $PATH if not there
  echo "$PATH" | grep -q '\./node_modules/\.bin' || export PATH="${PATH}:./node_modules/.bin"
  # finally lets make sure we can call NVM in the terminal
  alias nvm='PREFIX="" nvm'
  # if NPM is not in $PATH, reload it via NVM
  [ "$(npm --version &>/dev/null)" = "" ] && PREFIX='' nvm use $BASHRC_NODEJS_VERSION &>/dev/null

  # Set some Node REPL stuff here...

  # Enable persistent REPL history
  export NODE_REPL_HISTORY=~/.node_history
  # Allow 32³ entries; the default is 1000.
  export NODE_REPL_HISTORY_SIZE='32768'
  # Use sloppy mode by default, matching web browsers.
  export NODE_REPL_MODE='sloppy'
fi
export PREFIX=/usr # just in case

