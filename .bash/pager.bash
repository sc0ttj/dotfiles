# Pager settings

# This file is sourced by ~/.bash/main.bash

# SET $pager related vars
export PAGER='less'

# enable printing of UTF-8 sequences and chars
export LESSCHARSET='utf-8'

# set default settings for less:
#   -i      ignore case
#   -M      long prompt
#   -F      quit if one screen
#   -X      no init
#   -R      raw control chars (enable colours)
#   --tabs  set tab spacing
export LESS='-iMFXR --tabs=2'
