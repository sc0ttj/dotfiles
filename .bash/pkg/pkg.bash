# Pkg - puppy linux package manager
# see https://gitlab.com/sc0ttj/Pkg

[ ! -x /usr/sbin/pkg ] && return

# enable Pkg 1.9.21alpha TAB completion
export PKG_TAB_COMPLETION=true
. /etc/bash_completion.d/pkg 2>/dev/null

# enable suggest packages when last command not found
. /usr/share/pkg/command_not_found_handle.sh 2>/dev/null
