# BASH prompt settings

# This file is sourced by ~/.bash/main.bash

# Settings can be configured in ~/.bash/settings

# To show icons correctly, choose the 'DejaVuSansMono'
# nerd font in ~/.fonts as your default terminal font.

# Prompt info and features:
  # lots of colours
  # Nerd font icons, when running in X
  # responsive layout (2 line prompt on small screens)
  # fallback to simpler prompt (no icons) when no X
  # shows:
    # battery status
    # wifi status
    # current keyboard layout
    # current user
    # device type (laptop|desktop)
    # current hostname
    # sub-shell number
    # current date and time
    # current dir
    # filesize info of current dir
    # git repo status
    # status of last command


# if we have X, we are likely in a terminal emulator, and so
# *should* have our Nerd fonts installed, else use basic settings
function X_is_running {
  [ ! -z "$DISPLAY" ] && echo true || echo false
}

# In Bash version 4.0 or above, you can set the PROMPT_DIRTRIM
# variable for the shell. This limits the length of the tail
# end of the \w  and \W expansions to the specified number of
# path elements. Lets default it to 3:
export PROMPT_DIRTRIM=${BASHRC_PROMPT_DIRTRIM:-3}

# how many sub shells deep are we?
subshell_levels=''
[ "$BASHRC_PROMPT_SUBSHELLS" = true ] && subshell_levels=$(((SHLVL>1))&&echo $SHLVL)

git="$(which git)"

# show laptop icon if device is a laptop, else show a desktop icon:
# this icon is shown next to the hostname
function _host_icon {
  # leave now if disabled in ~/.bash/settings
  [ "$BASHRC_PROMPT_HOST"  != true ] && return 0
  [ "$BASHRC_PROMPT_ICONS" != true ] && return 0

  [ "$(which dmidecode)" = "" ] && return 0
  local device_type="$(dmidecode --string chassis-type 2>/dev/null)"
  echo "$device_type" | grep -qE 'Notebook|Netbook|Laptop' && echo '' && return
  echo ' '
}


function _battery_info {
  # leave now if disabled in ~/.bash/settings
  [ "$BASHRC_PROMPT_BATTERY" != true ] && return 0
  [ "$BASHRC_PROMPT_ICONS" != true ] && return 0

  # only show battery info if acpitool is installed
  [ "$(which acpitool)" = "" ] && return 0

  local battery_prompt=''
  local battery_icon=''
  local battery_percent_charged=''

  # get battery info for first available battery
  local battery_info="$(acpitool -b 2>/dev/null)"

  # get battery_level (int) and battery_status (charged|charging|discharging)
  local battery_status=$(echo "$battery_info" | grep -m1 ' Battery #' | head -1 | cut -f2 -d':' | cut -f1 -d',' | sed -e 's/[ ,%]//g')
  local battery_level=$(echo  "$battery_info" | grep -m1 ' Battery #' | head -1 | cut -f2 -d',' | cut -f1 -d'.' | sed -e 's/[ ,%]//g')

  [ "${battery_level}" = "" ] && return 0

  # show % charged (if enabled in settings)
  [ "$BASHRC_PROMPT_BATTERY_PERC" = true ] && battery_percent_charged="${battery_level}%"

  if [ "$BASHRC_PROMPT_ICONS" = true ];then
    # set the battery icon based on the remaining charge
    [ ${battery_level:-100} -lt 85 ] && battery_icon=""
    [ ${battery_level:-100} -lt 60 ] && battery_icon=""
    [ ${battery_level:-100} -lt 35 ] && battery_icon=""
    [ ${battery_level:-100} -lt ${BASHRC_PROMPT_BATTERY_WARNING_LIMIT:-15} ] && battery_icon=""
    charging_icon=""
  else
    battery_icon=''
    charging_icon=''
  fi

  # if < 20% and discharging, set flashing red low battery icon
  if [ "$battery_status" = 'discharging' ];then
    battery_prompt="${light_gray}${battery_icon}  ${battery_percent_charged}${no_color}"
    if [ ${battery_level} -lt ${BASHRC_PROMPT_BATTERY_WARNING_LIMIT:-15} ];then
      battery_prompt="${flashing_red}${battery_icon}  ${battery_percent_charged}${no_color}"
    fi

  # if battery charging, set yellow plug icon
  elif [ "$battery_status" = 'charging' ];then
    battery_prompt="${yellow}${charging_icon} ${battery_percent_charged}${no_color}"

  # if battery charged, set a full battery icon in green
  elif [ "$battery_status" = 'charged' ];then
    battery_prompt="${green}${battery_icon} ${no_color}"
  fi

  [ "$battery_prompt" != "" ] && echo -e "$battery_prompt"
}


# print current keyboard layout
function _kb_layout {
  [ "$(which setxkbmap)" = "" ] && return

  local kb_layout="$(setxkbmap -query | grep layout | sed -e 's/layout:[ ]*//g')"
  local kb_prompt=''

  kb_prompt="${light_gray}${kb_layout}-${kb_layout_icon}  "

  [ "$kb_layout" != "" ] && echo "$kb_prompt"
}


# shows wifi signal strength
function _wifi_signal {
  [ "$(which iwconfig)" = "" ] && return
  [ "$(iwconfig wlan0 2>/dev/null| grep -m1 'No such device')" != "" ] && return

  local signal_strength="$(iwconfig wlan0 2>/dev/null | grep -i --color quality|cut -f2 -d'='|cut -f1 -d' ' | sed 's|/|/ |')"
  local signal_as_percentage="$(echo ${signal_strength}' * 100' | bc -l 2>/dev/null | cut -b1-2)%"

  [ "$signal_strength" != "" ] && echo -e "${blue}${wifi_icon} ${signal_as_percentage}${no_color}  "
}


# git aware prompt, shows branch info, etc
function _git_prompt {
    [ "$git" = "" ] && return 0
    [ "$BASHRC_PROMPT_GIT" != true ] && return 0

    # get the git status of this repo
    local git_status="`LANG=C LC_ALL=C git status -unormal 2>&1`"
    local is_git_repo="`LANG=C LC_ALL=C  git rev-parse --is-inside-work-tree 2>/dev/null`"

    local red="\[\033[38;5;160m\]"
    local green="\[\033[38;5;40m\]"
    local yellow="\[\033[38;5;11m\]"
    local orange="\[\033[38;5;208m\]"
    local END_COLOR='\e[0m'

    local commit_ansi=''
    local branch_ansi=''
    local branch_status=''
    local commit_status=''

    # get number of new files added, tracked file modified, tracked files to be deleted
    local count_added="$(echo "$git_status" | grep 'new file:  ' | wc -l)"
    local count_modified="$(echo "$git_status" | grep -E 'modified:  |renamed:  ' | wc -l)"
    local count_deleted="$(echo "$git_status" | grep 'deleted:  ' | wc -l)"
    local count_stash="$(git stash list 2>/dev/null| grep ^stash | wc -l)"

    # if not in a git repo, skip creating the git prompt
    if [ "$is_git_repo" = true ];then

        # show green if branch is clean
        if [[ "$git_status" =~ 'nothing to commit' ]];then
            commit_ansi=$green
            commit_status='' # could use ✔ or ✓

        # show orange if untracked files present
        elif [[ "$git_status" =~ 'nothing added to commit but untracked files present' ]];then
            commit_ansi=$orange
            commit_status='' # could use ?

        # show orange if changes to tracked files exist, staged, but not yet committed
        elif [[ "$git_status" =~ 'Changes to be committed' ]];then
            commit_ansi=$orange
            commit_status=''

        # show red if tracked files are changed, and changes not staged
        elif [[ "$git_status" =~ 'Changes not staged for commit' ]];then
            commit_ansi=$red
            commit_status=''

        # else if not showing "nothing to commit", its unclean somehow, show red
        elif [[ ! "$git_status" =~ 'nothing to commit' ]];then
            commit_ansi=$red
            commit_status='!' # could use ✘, ✗, ✖, or ✕
        fi


        # we will append the number of files modified, deleted or added
        # to the end of the branch name & status

        # append number of deleted files (files to be deleted on commit)
        if [ $count_deleted -gt 0 ];then
          commit_status="-${count_deleted} ${commit_status}"
        fi

        # append number of new files staged to commit
        if [ $count_added -gt 0 ];then
          commit_status="+${count_added} ${commit_status}"
        fi

        # append number of modified files (tracked and modified)
        if [ $count_modified -gt 0 ];then
          commit_status="M${count_modified} ${commit_status}"
        fi

        # if any untracked files, append '..?' to git prompt
        if [[ "$git_status" =~ 'Untracked files' ]];then
            commit_status="${commit_status}${red}..?"
        fi

        # if any stashes, show stash count
        if [ $count_stash -gt 0 ];then
            commit_status=" ${commit_status} ${END_COLOR}${count_stash}⚑"
        fi



        # show how many commits branch is ahead or behind remote branch
        if [[ "$git_status" =~ 'Your branch is ahead' ]];then
            count="$(echo "$git_status" | grep -m1 ahead | sed -e 's/.*ahead.*by //g' -e 's/ commit.*//g')"
            branch_ansi=$orange
            branch_status="▲${count} " # could use ↑

        elif [[ "$git_status" =~ 'Your branch is behind' ]];then
            count="$(echo "$git_status" | grep -m1 behind | sed -e 's/.*behind.*by //g' -e 's/ commit.*//g')"
            branch_ansi=$red
            branch_status="▼${count} " # could use ↓

        elif [[ "$git_status" =~ 'have diverged' ]];then
            count_ahead="$(echo "$git_status"  | grep '^and have' | cut -f3 -d' ')"
            count_behind="$(echo "$git_status"  | grep '^and have'| cut -f5 -d' ')"
            branch_ansi=$red
            branch_status="▾${count_behind}▴${count_ahead} " # could use ↓,↑

        # else, if no commit ahead or behind, show green
        elif [[ "$git_status" =~ 'Your branch is up-to-date' ]];then
            branch_ansi=$green
            branch_status=''
        fi


        # show commit instead of branch name if not on a branch
        if [[ "$git_status" =~ On\ branch\ ([^[:space:]]+) ]];then
            branch=${BASH_REMATCH[1]}
            #test "$branch" != master || branch=' '
        else
            # Detached HEAD.  (branch=HEAD is a faster alternative.)
            local branch="(`git describe --all --contains --abbrev=4 HEAD 2> /dev/null ||
                echo HEAD`)"
        fi

        # adds a separator between branch and commit info
        #if [ "$commit_status" != '' ];then
        #  branch_status="${branch_status}${END_COLOR}|"
        #fi

        echo -n "${branch_ansi}${github_icon} ${branch} ${branch_status}${commit_ansi}${commit_status}${END_COLOR}" | sed -e 's/  / /g'
    fi
}


function _prompt_char {
  # assume regular user, prompt char is > and
  # user name in prompt is shown in white
  local prompt_char='$'
  # this var will hold colored prompt_char:
  # if last cmd was error, show red, else show green
  local prompt_symbol=''

  # if root, set the prompt character (indicating where
  # to type) to $
  if [ "$UID" = '0' -a "$EUID" = '0' ];then
    prompt_char='#'
  fi

  # assume last command was good, set $prompt_char green,
  prompt_symbol="${green}${prompt_char}"

  # if the last command (passed in as $1) exited with
  # a non-zero (error) status
  if [ $1 != 0 ];then
    # set to red
    prompt_symbol="${red}${prompt_char}"
  fi

  # end colorised output of prompt_symbol
  prompt_symbol="${prompt_symbol}${no_color}"

  echo "$prompt_symbol"
}



# if no X, override icons settings and disable them,
# as we probably are not using Nerd Fonts
[ $(X_is_running) = false ] && BASHRC_PROMPT_ICONS=false

# set the default icons (also see host_icon, battery_info funcs below)
user_icon=''
host_icon=''
wifi_icon='wifi:'
subshells_icon='sh:'
kb_layout_icon='kb'
date_icon=''
time_icon=''
wdir_icon=''
files_icon=''
github_icon=''
if [ "$BASHRC_PROMPT_ICONS" = true ];then
  user_icon=''
  host_icon="$(_host_icon)"
  wifi_icon=''
  subshells_icon=' '
  kb_layout_icon=''
  date_icon=''
  time_icon=''
  wdir_icon=''
  files_icon=''
  github_icon=''
fi



function _prompt_command {
  # get return status of last command (must do this
  # first, or we'll get the wrong return code)
  local EXIT="$?"

  # set some colours
  local no_color=""
  local blue=""
  local gray=""
  local green=""
  local purple=""
  local orange=""
  local red=""
  local yellow=""
  local white=""
  local light_blue=""
  local light_gray=""
  local light_green=""
  local light_purple=""
  local light_yellow=""
  local bright_white=""
  local flashing_red=""
  if [ "$BASHRC_PROMPT_COLORS" = true ];then
    no_color="\[\033[0m\]"
    blue="\[\033[38;5;27m\]"
    gray="\[\033[38;5;245m\]"
    green="\[\033[38;5;34m\]"
    purple="\[\033[38;5;165m\]"
    orange="\[\033[38;5;214m\]"
    red="\[\033[38;5;160m\]"
    yellow="\[\033[38;5;3m\]"
    white="\[\033[38;5;15m\]"
    light_blue="\[\033[38;5;39m\]"
    light_gray="\[\033[38;5;250m\]"
    light_green="\[\033[38;5;112m\]"
    light_purple="\[\033[38;5;141m\]"
    light_yellow="\[\033[38;5;11m\]"
    bright_white="\[\033[38;5;255m\]"
    flashing_red="\[\033[5m\]\[\033[38;5;160m\]"
  fi

  # set color of users name, white if regular user
  # or red if root user
  local user_color="${white}"
  if [ "$UID" = '0' -a "$EUID" = '0' ];then
    user_color="${red}"
  fi

  local current_date="$(date +'%a %d %b')"
  local current_time="$(date +'%R')"

  local file_count="$(\ls -1 2>/dev/null | wc -l | sed 's: ::g')"
  local dir_size_in_Kb="$(\ls -lah 2>/dev/null | grep -m1 total | sed 's/total //')"


  # responsive break point for smaller screens/terminals
  local br=""
  [ ${COLUMNS:-120} -lt ${BASHRC_PROMPT_BREAKPOINT:-120} ] && br='\n'

  local user='' host='' battery='' subshells='' kb_layout='' datenow='' timenow='' wdir='' files='' fsize=''

  [ $BASHRC_PROMPT_USER = true ]      &&  user="${user_color}${user_icon} \u "
  [ $BASHRC_PROMPT_HOST = true ]      &&  host="${orange}${host_icon} \h  "
  [ $BASHRC_PROMPT_BATTERY = true ]   &&  battery="$(_battery_info)  "
  [ $BASHRC_PROMPT_SUBSHELLS = true ] &&  subshells="${green}${subshells_icon}${subshell_levels:-0}  "
  [ $BASHRC_PROMPT_KBLAYOUT = true ]  &&  kb_layout="$(_kb_layout)  "
  [ $BASHRC_PROMPT_DATE = true ]      &&  datenow="${purple}${date_icon} ${current_date}  "
  [ $BASHRC_PROMPT_TIME = true ]      &&  timenow="${light_purple}${time_icon} ${current_time}  ${br}"
  [ $BASHRC_PROMPT_WDIR = true ]      &&  wdir="${light_blue}${wdir_icon} \w  "
  [ $BASHRC_PROMPT_DIRINFO = true ]   &&  files="${yellow}${files_icon} ${file_count} files "
  [ $BASHRC_PROMPT_FILESINFO = true ] &&  fsize="${light_yellow}${dir_size_in_Kb}b${no_color}  "

  # set last char of prompt: # or $ depending on user,
  # and red if last command errored, else green
  # ($EXIT is te return status of the users last run command)
  local prompt_char=$(_prompt_char $EXIT)

  # if in a basic terminal, no X, no 256 colors, no Nerd fonts,
  # just set a more basic prompt (like above, but no icons):
  if [ $(X_is_running) = false ];then

    # no X, no Nerd fonts, set basic prompt
    PS1="${no_color}\n${red}\u${no_color}@${orange}\h  ${blue}sh:${subshell_levels:-0}  ${purple}$(date +'%a %d %b') ${light_purple}$(date +'%R')  ${br}${green}\w  ${light_green}${file_count} files ${yellow}${dir_size_in_Kb}b$(_git_prompt)${no_color}\n# "

  else

    # set full prompt, with colors and icons
    PS1="\n${user}${host}${battery}$(_wifi_signal)${subshells}${kb_layout}${datenow}${timenow}${wdir}${files}${fsize}$(_git_prompt)${no_color}\n${prompt_char:-$} "

  fi
}

PROMPT_COMMAND=_prompt_command

