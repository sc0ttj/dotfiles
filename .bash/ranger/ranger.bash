# Ranger - terminal file manager
#======================================================================

[ "$(which ranger)" = "" ] && return

# allow file previews for the root user: this will
# patch ranger so it supports the 'root_preview_files'
# option, which is set in ~/.config/ranger/rc.conf

already_patched="$(cat /usr/local/lib/python2.7/dist-packages/ranger/core/main.py 2>/dev/null | grep -m1 -q 'ENABLE PREVIEWS FOR ROOT TOO' && echo true || echo false)"

if [ "$BASHRC_RANGER_ROOT_PREVIEWS" = true -a "$already_patched" = false ];then
  if [ -f /usr/local/lib/python2.7/dist-packages/ranger/core/main.py ];then
    patch /usr/local/lib/python2.7/dist-packages/ranger/core/main.py ~/.bash/ranger/main.py.patch &>/dev/null
    patch /usr/local/lib/python2.7/dist-packages/ranger/container/settings.py ~/.bash/ranger/settings.py.patch &>/dev/null
  fi
fi

# use only the config files in ~/.config/ranger
export RANGER_LOAD_DEFAULT_RC=false

# Automatically change the directory in console/temrinal after closing ranger
rangr() {
	  echo -ne "\e]2; Ranger \a"
    tempfile="$(mktemp -t tmp.XXXXXX)"
    if [ -z "${RANGER_LEVEL}"  ];then
        $(which ranger) --choosedir="$tempfile" "${@:-$(pwd)}"
        test -f "$tempfile" &&
        if [ "$(cat -- "$tempfile")" != "$(echo -n `pwd`)" ]; then
            builtin cd -- "$(cat "$tempfile")"
	      fi
	      rm -f -- "$tempfile"
	  else
	      exit
	  fi
	  echo -ne "\e]2; Terminal \a"
}
export -f rangr

# add shortcuts
alias r=rangr
alias @=rangr
