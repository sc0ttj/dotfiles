# Redshift
#############################################

[ "$(which redshift)" = "" ] && return

alias dayscreen='redshift -O 5500K'
alias nightscreen='redshift -O 4300K'
