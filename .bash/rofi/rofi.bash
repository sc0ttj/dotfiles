# Rofi
#======================================================================

# exit now if rofi not installed
[ "$(which rofi)" = "" ] && return

# file browser using rofi
function rofifb {
  rofi -kb-row-up "Up,Control+p,Shift_L+ISO_Left_Tab,Shift+ISO_Left_Tab," \
       -show file \
       -modi file:~/.config/rofi/rofi-file-browser.sh
}

# spotlight-esque Run program launcher
function rofirun {
  rofi -kb-row-up "Up,Control+p,Shift_L+ISO_Left_Tab,Shift+ISO_Left_Tab," \
      -combi-modi ssh,drun,run \
       -display-drun '' \
       -display-run '' \
       -display-combi 'Run' \
       -show combi
}

# a dmenu-like rofi command (mainly for reference)
dmenu() {
  rofi -kb-row-up "Up,Control+p,Shift_L+ISO_Left_Tab,Shift+ISO_Left_Tab," \
       -show run -modi run -location 1 -width 100 \
  		 -lines 5 -line-margin 1 -line-padding 2 \
  		 -separator-style none -font "mono 14" -columns 1 -bw 0 \
  		 -disable-history \
  		 -hide-scrollbar \
  		 -color-window "#111111, #111111, #b1b4b3" \
  		 -color-normal "#111111, #b1b4b3, #111111, #005577, #b1b4b3" \
  		 -color-active "#111111, #b1b4b3, #111111, #007763, #b1b4b3" \
  		 -color-urgent "#111111, #b1b4b3, #111111, #77003d, #b1b4b3" \
  		 -kb-row-select "Tab" -kb-row-tab ""
}

