# SSHRC
######################################################################

# replace ssh with sshrc, so it auto-copies our 
# local dot files from ~/.sshrc and ~/.sshrc.d/ 
# to the remote env when we login.
alias ssh=sshrc

