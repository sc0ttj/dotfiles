# Vultr.com - my web hostng server stuff
######################################################################

if [ "$(which vultr)" != "" ];then

  # https://zero.support (zs)
  zs_subid='13482157'

  # setup aliases for each server:
  # zs (zero.support)
  alias zs_info='vultr server show ${zs_subid}'
  alias zs_start='vultr server reboot ${zs_subid}'
  alias zs_reboot='vultr server reboot ${zs_subid}'
  alias zs_halt='vultr server halt ${zs_subid}'
  # (other servers here)

fi

