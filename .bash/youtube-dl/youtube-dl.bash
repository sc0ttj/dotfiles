# Youtube-dl
#####################################################################

[ "$(which youtube-dl)" = "" ] && return

alias ytdl="youtube-dl --add-metadata -ic"    # Download video link

alias ytadl="youtube-dl --add-metadata -xic"  # Download only audio

alias mp3dl='youtube-dl --ignore-config --extract-audio \
  --audio-format "mp3" --audio-quality 0 --embed-thumbnail \
  --add-metadata --metadata-from-title "%(artist)s - %(title)s" \
  --output "%(title)s.%(ext)s"'

