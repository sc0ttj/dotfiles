# When Bash is invoked as an interactive login shell, 
# or as a non-interactive shell with the --login option, 
# it first reads and executes commands from the file 
# /etc/profile, if that file exists.
#
# After reading that file, it looks for ~/.bash_profile, 
# ~/.bash_login, and ~/.profile, in that order, and reads 
# and executes commands from the first one that exists 
# and is readable. 
#
# The --noprofile option may be used when the shell is 
# started to inhibit this behavior.
#
# When an interactive login shell exits, or a non-interactive 
# login shell executes the exit builtin command, Bash reads 
# and executes commands from the file ~/.bash_logout, if it exists. 


if [ -f ~/.bashrc ]; then . ~/.bashrc; fi

export PATH="$HOME/.cargo/bin:$PATH"
