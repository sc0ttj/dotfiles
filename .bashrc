# Debian Stretch config
###########################################################

# exit if not sourced
[[ $0 = "$BASH_SOURCE" ]] && exit 1

# leave if not bash
[ -z "$BASH_VERSION" ] && exit 1

# If not running interactively, don't do anything
[[ $- != *i* ]] && exit 1
# same, but to be safe
[ -z "$PS1" ] && exit 1

##########################################################

# get system ENV defaults
[ -f /etc/profile ] && . /etc/profile

# get latest system-wide key mappings
[ -f /etc/inputrc ] && bind -f /etc/inputrc

# get latest user-created key mappings
[ -f ~/.inputrc ] && bind -f ~/.inputrc

###########################################################

# get custom user settings
[ -f ~/.bash/main.bash ] && source ~/.bash/main.bash

return 0

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
