#!/usr/bin/env sh




# ranger supports enhanced previews.  If the option "use_preview_script"
# is set to True and this file exists, this script will be called and its
# output is displayed in ranger.  ANSI color codes are supported.

# NOTES: This script is considered a configuration file.  If you upgrade
# ranger, it will be left untouched. (You must update it yourself.)
# Also, ranger disables STDIN here, so interactive scripts won't work properly

# Meanings of exit codes:
# code | meaning    | action of ranger
# -----+------------+-------------------------------------------
# 0    | success    | success. display stdout as preview
# 1    | no preview | failure. display no preview at all
# 2    | plain text | display the plain content of the file
# 3    | fix width  | success. Don't reload when width changes
# 4    | fix height | success. Don't reload when height changes
# 5    | fix both   | success. Don't ever reload
# 6    | image      | success. display the image $cached points to as an image preview
# 7    | image      | success. display the file directly as an image

# Meaningful aliases for arguments:
path="$1"            # Full path of the selected file
width="$2"           # Width of the preview pane (number of fitting characters)
height="$3"          # Height of the preview pane (number of fitting characters)
cached="$4"          # Path that should be used to cache image previews
preview_images="$5"  # "True" if image previews are enabled, "False" otherwise.

maxln=200    # Stop after $maxln lines.  Can be used like ls | head -n $maxln

# Find out something about the file:
mimetype=$(file --mime-type -Lb "$path")
filetype=$(file "$path")
extension=$(/bin/echo "${path##*.}" | awk '{print tolower($0)}')

# Functions:
# runs a command and saves its output into $output.  Useful if you need
# the return value AND want to use the output in a pipe
try() { output=$(eval '"$@"'); }

# writes the output of the previously used "try" command
dump() { /bin/echo "$output"; }

# a common post-processing function used after most commands
trim() { head -n "$maxln"; }

# wraps highlight to treat exit code 141 (killed by SIGPIPE) as success
safepipe() { "$@"; test $? = 0 -o $? = 141; }

# Image previews, if enabled in ranger.
if [ "$preview_images" = "True" ]; then
    case "$mimetype" in
        # Image previews for SVG files, disabled by default.
        image/svg+xml)
           rsvg-convert -o "$cached"  "$path" && exit 6 || exit 1;;
        # Image previews for image files. w3mimgdisplay will be called for all
        # image files (unless overriden as above), but might fail for
        # unsupported types.
        image/*)
            #fbi "$path" && exit 6 || exit 1;;
            exit 7;;
        # Image preview for video, disabled by default.:
        video/*)
            ffmpegthumbnailer -i "$path" -o "$cached" -s 0 && exit 6 || exit 1;;
    esac
fi

case "$extension" in
    # Archive extensions:
    7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|\
    pet|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip)
        try als "$path" && { dump | trim; exit 0; }
        try acat "$path" && { dump | trim; exit 3; }
        try tar vvtf "$path" 2>/dev/null && { dump | trim; exit 3; }
        try atool --list --each -- "$path" && { dump | trim; exit 3; }
        try bsdtar -lf "$path" && { dump | trim; exit 0; }
        try uextract -l "$path" && { dump | trim; exit 3; }
        try pkg contents "$path" && { dump | trim; exit 3; }
        exit 1;;
    rar)
        try unrar -p- lt "$path" && { dump | trim; exit 0; } || exit 1;;
    pdf)
        try pdftotext -l 10 -nopgbrk -q "$path" /tmp/pdffile && cat /tmp/pdffile && \
          { dump | trim | fmt -s -w $width; exit 4; } || exit 1;
        try pdftoppm "$path" /tmp/pdfimage -jpeg -singlefile -scale-to 700 && { fbi /tmp/pdfimage && exit 6 || exit 1; }
        try green -fullscreen "$path" && { dump | trim | fmt -s -w $width; exit 0; }
        ;;
    epub)
        try epub2txt "$path" && { dump | trim | exit 4; } || exit 1;;
    doc)
        try catdoc "$path" && { dump | trim | exit 0; }
        exit 1;;
    xls)
        try xls2csv "$path" && { dump | trim | exit 0; }
        exit 1;;
    torrent)
        try ctorrent -x "$path" && { dump | trim; exit 5; } || exit 1;;
        #try transmission-show "$path" && { dump | trim; exit 5; } || exit 1;;
    htm|html|xhtml)
        #try links2 "$path" && { dump | trim | fmt -s -w $width; exit 4; }
        #try lynx "$path" && { dump | trim | fmt -s -w $width; exit 4; }
        #try elinks "$path" && { dump | trim | fmt -s -w $width; exit 4; }
        try w3m "$path" && { dump | trim | fmt -s -w $width; exit 4; }
        ;; # fall back to highlight/cat if the text browsers fail
esac


case "$mimetype" in

    # Syntax highlight for text files:
    text/* | */xml)
        # custom fix: shell highlight Ash scripts too
        if [ "$(echo "$filetype" | grep -E 'ash script|shell script')" != "" ];then
          try safepipe bat --color=always --paging=never -l sh "$path" && { dump | trim; exit 5; }
        else # continue as normal
          try safepipe bat --color=always --paging=never "$path" && { dump | trim; exit 5; }
          try safepipe pygmentize -f terminal256 -O style=monokai -g "$path" && { dump | trim; exit 5; }
          try safepipe highlight --out-format=ansi --style=molokai --font-size=11 -t 2 "$path" && { dump | trim; exit 5; }
          try safepipe ccat "$path" && { dump | trim; exit 5; }
        fi
        exit 2;;

    # Ascii-previews of images:
    image/*)
        fbi "$path" && exit 6 || exit 1;;
        #img2txt --gamma=0.6 --width="$width" "$path" && exit 4 || exit 1;;

    # Display information about media files:
    video/* | audio/*)
        exiftool "$path" && exit 5
        # Use sed to remove spaces so the output fits into the narrow window
        try mediainfo "$path" && { dump | trim | sed 's/  \+:/: /;';  exit 5; } || exit 1;;
esac

exit 1
