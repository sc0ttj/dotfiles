# User Fonts

In this dir is the Nerd fonts I use to enable icons in the terminals.

The fonts included are:

```
DejaVuSansMono Nerd Font
NotoEmoji-Regular
```

To use these fonts, do the following:

1. In a terminal, run the following:

```
fc-cache -rv
```

2. To enable icons in the terminal, open your terminal emulator config files or settings menu  
   (maybe `Ctrl-LeftClick`, or `Ctrl-RightClick`), and select the font  
   `DejaVu Sans Mono Nerd Font Complete` (size 14 is good).

3. And/or edit `~/.Xdefaults` or `~/.Xresources` to edit settings for Xterm,
   Rxvt, Urxvt and so on.  
   Example:

```
! See https://wiki.archlinux.org/index.php/X_Logical_Font_Description
rxvt.font:  xft:DejaVuSansMono Nerd Font:style=Book:pixelsize=14:antialias=false:hinting=true
urxvt.font: xft:DejaVuSansMono Nerd Font:style=Book:pixelsize=14:antialias=false:hinting=true
```

4. Run `reload` in yout terminal, or restart your terminal emulator.
