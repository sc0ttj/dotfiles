# README

My BASH settings for Puppy Linux (Stretch 7.5).

## Summary

- lots of colours in terminal (with monokai code-highlighting where possible)
- icons in terminal (using DejaVu Sans Nerd Font, see ~/.fonts)
- powerful 'cd' command replacement
- nicer defaults for many commands (ls, less, ps, mkdir, mount, diff, etc)
- powerful BASH prompt (shows user, host, wifi signal, date, time, dir info)
- Git aware replacements for: ls, cat, cd, prompt, grep
- modern text-editor in the console (micro)
- awesome TAB completion settings
- great key bindings and useful shortcuts
- lots of useful aliases, functions and scripts in ~/bin/
- bash TAB completion scripts for many common commands
- fixes some missing ENV vars and keybindings
- supports many 3rd-party tools (nearly all are optional)

## Download

Download: `git clone -b puppy-stretch-7.5 https://gitlab.com/sc0ttj/dotfiles ~/dotfiles`

## Config

Open `~/dotfiles/.bash/settings` in a text editor.

## Install

1. Run the installer, `~/dotfiles/.bash/installer.sh`.

This will install everything in `~/dotfiles/` to `~/`.
Then a bunch of stuff will be installed using `git`, `pip` and `pkg`, including:

- `pkg` - a powerful command-line package manager for Puppy Linux
- `mlterm` - a utf-8 terminal with tabs and great image support
- `micro` - a modern text editor for the console (supports plugins, linters, multiple cursors)
- `ranger` - a console based file manager with excellent file previews
- `fzf` - a powerful and versatile fuzzy file finder and previewer
- `ag` - like grep, but faster, and support Git ignores, more
- `bat` - drop-in replacement for `cat`, supports syntax highlighting, line numbers, Git diff in gutter
- `exa` - powerful `ls` replacement, supports icons, .gitignore, shows file Git status, more
- `fd` - a faster and easier than `find`, supports .gitigore and more (used by fzf)
- `nvm` - manage and install various NodeJS and NPM versions
- `nodejs` - JavaScript interpreter, with `npm` package manager included
- `tldr` - simplified `manpages` with useful examples
- `cht.sh` - searchable cheatsheets
- `how2doi` - search the stack exchange network from the console
- `mpsyt` - for console youtube viewing
- `sshrc` - automatically copy your environment stuff from ~/.sshrc/ to your remote servers
- `rofi` - fast, searchable pop-up app launcher.. highly customisable (see `~/.config/rofi/`)
- `xlunch` - another searchable pop-up app launcher (see ~/.config/xlunch/, ~/bin/xlunchr)
- and more...

2. Run `source ~/.bashrc` or restart your terminal.

## Usage

Reload your terminal settings with **this command**:

```
reload
```

This will reload your current BASH shell and as update font cache and settings in `~/.Xdefaults` and `~/.Xresources`.

It is recommended to use `reload` over simply `source ~/.bashrc`.

# Features

## Terminal Key bindings:

#### Awesome TAB behaviour:

- press `TAB` once to list completions, leaving input as is (partial),
- press `TAB` twice to replace input with first completion option,
- press `TAB` again to get next completion option,
- press `Shift-TAB` to get previous completion option

Plus TAB completion for many commands, including `pkg`, `git`, and more.

#### Word navigation

- `Ctrl-Left`: word left
- `Ctrl-Right`: word right
- `Alt-Left`: word left
- `Alt-Right`: word right
- `Alt-Delete`: delete whole next word
- `Alt-Backspace`: delete whole previous word
- `Alt-b`: skip backwards by one word
- `Alt-f`: skip forwards by one word
- `Alt-u`: uppercase currently highlighted word
- `Alt-l`: lowercase currently highlighted word

#### Edit `$PATH` and `$LD_LIBRARY_PATH`

- `Ctrl-x,p` edit and save $PATH
- `Ctrl-x,l`: edit and save $LD_LIBRARY_PATH
- `Ctrl-p`: copy current line to clipboard

#### Use `fzf` to open & edit files

- `Ctrl-o`: open files.. searchable, with file previews (use `Ctrl-y` to copy filename)
- `Alt-c`: cd to dir.. fzf popup listing dirs, and cd into chosen dir
- `Alt-t`: choose file and print to stdout

#### Search `git` repos with `fzf`

- `Alt-g,b`: fuzzy search and choose a git branch
- `Alt-g,c`: fuzzy search and checkout a git commit
- `Alt-g,h`: browse and preview Git repo history

#### Searching command history

- `Ctrl-h`: search command history for currently printed cmd
- `Shift-Up`: search previous matching commands CONTAINING the current <command>
- `Shift-Down`: search next matching commands CONTAINING the current <command>

Also see `~/.inputrc` and `~/.bash/keybindings.bash` for more.

## JWM (window manager) Keybindings

#### Window management

- `Ctrl+ WinKey`: load the regular JWM main menu
- `WinKey + Left`: move window left
- `WinKey + Right`: move window right
- `WinKey + Up`: move window top
- `WinKey + Down`: move window bottom
- `WinKey + SPACE`: toggle maximize window

#### Pop-up app launchers

- `Alt + SPACE`: load a searchable app launcher, based on `rofi`
- `Ctrl + SPACE`: load a searchalbe app launcher, based on `rofi`
- `Ctrl+ Alt + SPACE`: load a searchable app launcher, based on `xlunch`

Note, the Windows key (WinKey) is called `Mod4` in JWMs config files.  
Look in `~/.jwm/jwmrc` for more key bindings.

## What's included?

#### Main file, sourced by `.bashrc`:

`~/.bash/main.bash`

This file set lots of environment variables such as $PATH, $EDITOR, $MANPAGES and more.
It also sources lots of other files, and is the 'loader' script, which gets sourced by `.bashrc`.

#### Bash prompt:

`~/.bash/prompt.bash`

This is a powerful, colourful, informative and Git-aware BASH prompt with icons.

- shows user, host, wifi signal, battery charge, dir info, git status and more
- sources settings from `~/.bash/settings`

#### Bettercd:

A powerful `cd` command replacement, with the following features:

- print dir contents after change dir (using `exa`, `colorls`, ..., or regular `ls`)
- built-in implementation of dir-based ENV settings (uses `.env` and `.env/leave` files, like [Autoenv](https://github.com/kennethreitz/autoenv))
- supports many options, see `cd --help`
- can define your own `ls` replacements to be used
- sources settings from `~/.bash/settings`

#### Aliases:

See `~/.bash/aliases`.
This file sets more useful defaults for many built-in commands.

#### Functions:

See `~/.bash/functions.bash`
This file provides useful functions you can use on the command-line.

#### Fzf stuff:

The default Fzf setup has been customised in the following ways:

- `fzf` searches respect `.gitignore` files (using `fd`)
- added many functions using `fzf` in `~/.bash/fzf/`
- all keybindings work out of the box (Ctrl-O, Alt-C, Alt-I)

Look in `~/.fzf/`, `~/.bash/fzf/` and `~/.config/.fzf`

#### Settings for BASH prompt and `cd` command:

See `~/.bash/settings`.
You can define settings for your BASH prompt and custom `cd` function here.

#

# What problems do these dotfiles solve?

#### Easier on the eye:

- lots of colours for quick info from `ls`, `grep`, `cat`, `ag`, more
- syntax highlighting (monokai theme)
- icons in terminal, to show filetypes
- better diffs, with bold colours showing changes within each line
- nicer prompt with icons, colours and env info and more
- Git aware replacements for: ls, cat, cd, prompt, grep

#### Automatic dir-based environments:

- just put a `.env` file in any dir to easily set ENV vars on a per-directory basis
- simply `cd` into a dir containing a `.env` and it will be sourced automatically
- then `cd` to anywhere else, and `env.leave` will be sourced (if found)
- uses Autoenv (if installed), or a built-in fallback if not
- see `~/.bash/func.cd.bash` for more info

#### Faster, easier file management:

- more powerful `cd` command, prints dir contents and more. See `cd -h`
- exa - `ls` with icons and colours enabled
- ranger (very powerful console-based file manager)
- fzf - fuzzy file and dir searching in the console
- silverseacher (ag) - search file contents
- plus many custom aliases and functions (see ~/.bash/)

#### Powerful, 256 colour, UTF-8 Terminal:

##### mlterm

- with mlterm GUI config menus (Ctrl-LeftButton, Ctrl-RightButton)
- supports multiple tabs (Ctrl-T for new tab, Ctrl-TAB to switch tabs)
- great support for UTF-8 inputs and languages
- using DejaVu Sana Mono (Nerd Font) for wide script and icon support

##### lxterminal

- also included in Puppy Stretch 7.5
- it also supports Nerd fonts, etc, set through menu (Ctrl-LeftButton)

#### Better in-terminal text editing:

- micro editor: a terminal editor with modern, user-friendly features and key bindings

#### Better remote server setup

- put files or symlinks (like .bashrc, .inputrc, etc) in `~/.sshrc.d/`
- or add stuff in the file `~/.sshrc`
- they will be loaded/sourced automatically when you SSH into a remote server
