#!/bin/bash

# use KVM to boot ISOs
# requires the 'ovmf' package to be installed

# usage:  boot_efI_iso.sh /path/to/file.iso


if [ "$(which kwm)" = "" ];then
  echo "The 'kvm' binary not found."
  exit 1
fi

if [ ! -f "$1" ];then
  echo "Error: ${1:-first option} must be a file"
  exit 1
fi

kvm \
  -bios /usr/share/qemu/OVMF.fd \
  -drive format=raw,file="$1" \
  -serial stdio \
  -m 2G \
  -cpu host \
  -smp 2

exit $?
