#!/bin/bash

# use KVM to boot ISOs

# usage:  boot_iso.sh /path/to/file.iso


if [ "$(which kwm)" = "" ];then
  echo "The 'kvm' binary not found."
  exit 1
fi

if [ ! -f "$1" ];then
  echo "Error: ${1:-first option} must be a file"
  exit 1
fi

kvm \
  -drive format=raw,file="$1" \
  -serial stdio \
  -m 2G \
  -cpu host \
  -smp 2

exit $?
