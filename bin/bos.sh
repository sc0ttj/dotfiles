#!/bin/bash

# config
set -euo pipefail
output_dir="./"


if [ "${1-}" = '-h' ] || [ "${1-}" = '--help' ] || [ "${1-}" = '-help' ]
then
  cat << EOF

Bash on Steroids:

  - enables embedding BASH code into HTML much more easily
  - all GET and POST vars made as available as regular shell vars
  - for example, POST['foo'] will be available as \$foo in your shell code

Usage:

  bos.sh  # no options needed

  This will generate Bash CGI files from any *.bos files in the current
  dir. You should move the generated scripts to your cgi-bin folder.

How to:

  - write your HTML files as usual, save with a .bos extension
  - you can include shell code in your .bos files using:

     <? [your shell code] ?>

     and

     <?bash
       [your shell code]
     ?>

  - Example *.bos file:

       <html>
       <head><title>foo</title></head>
       <body>
         <p>some words</p>
         <? [your shell code here] ?>
         <p> more words</p>
         <div>
           <?bash
             [your shell code here]
           ?>
         </div>
       </body>
       </html>

EOF

fi



bos() {
  tee "$2" >>/dev/null <<EOF
#!/bin/bash

echo Content-type: text/html
echo ""

## make POST and GET stings
## as bash variables available
if [ ! -z \$CONTENT_LENGTH ] && [ "\$CONTENT_LENGTH" -gt 0 ] && \\
   [ \$CONTENT_TYPE != "multipart/form-data" ]; then
  read -n \$CONTENT_LENGTH POST_STRING <&0
  eval \`echo "\${POST_STRING//;}"|tr '&' ';'\`
fi
eval \`echo "\${QUERY_STRING//;}"|tr '&' ';'\`

## decode URL-encoding
urldecode() { : "\${*//+/ }"; echo -e "\${_//%/\\x}"; }

EOF


  IFS=''
  flag=0
  lc=0
  export txt=''
  while read -r line;do
  	lc=$(( lc+1 ))
  	# Start and end tags in same line
  	if echo "$line"| grep -qe "<?.*?>"  -qe "<?bash.*?>";then
  		if [[ $flag -eq 0 ]];then
  			line=${line//<?bash/<?}
  			pre=$(echo -e "$line"  |  sed -e 's/<?.*//')
  			comm=$(echo -e "$line" | sed -e 's/^.*<? //' | sed -e 's/?>.*//')
  			post=$(echo -e "$line" | sed -e 's/^.*?>//')

  			if [[ -n "$pre" ]];then
  				pre=${pre//\\/\\\\}
  				mod_pre=$(echo -e "$pre" |sed 's/"/\\\"/g')
  				mod_pre=${mod_pre//\$/\\$}
  				echo -e "echo  \"$mod_pre\"" >>"$2"
  			fi
  			echo -e "$comm" >> "$2"
  			if [[ -n "$post" ]];then
  				post=${post//\\/\\\\}
  				mod_post=$(echo -e "$post" | sed 's/"/\\\"/g')
  				mod_post=${mod_post//\$/\\$}
  				echo -e "echo  \"$mod_post\"" >>"$2"
  			fi
  			continue
  		else
  			echo  "ERROR : Non-terminated &lt;?bash or &lt;? in file $1 (Line $lc) "
  			exit 1
  		fi
  	fi
  	# Start tag
  	if echo "$line" | sed 's/^ *//'| grep -q "^<?bash\\|^<?";then
  		if [[ $flag -eq 0 ]];then
  			flag=1
  			line=${line/<?bash}
  			echo -e "${line/<?}" >> "$2"
  		else
  			echo  "ERROR : Non-terminated &lt;?bash or &lt;? in file $1 (Line $lc) "
  			exit 1
  		fi
  		continue;
  	fi
  	# End tag
  	if echo "$line"|grep -q "?>$";then
  		if [[ $flag -eq 1 ]];then
  			flag=0
  			echo "${line/?>}" >> "$2"
  		else
  			echo "ERROR : Unmatched ?&gt; in file $1 (Line $lc)"
  			exit 1
  		fi
  	 continue;
  	fi
     # Take action on current line based on flag
      if [[ $flag -eq 0 ]];then
          line=${line//\\/\\\\}
          modline=$(echo  -e "$line" | sed -e 's/"/\\\"/g')
          modline=${modline//\$/\\$}
          echo -e "echo  \"$modline\"" >>"$2"
      else
          echo -e "${line}" >> "$2"
      fi
  done < "$1"
}


for file in *.bos
do
  if [[ "$file" -nt ${output_dir}"${file//.bos/.sh}" ]];then
     echo -e "Converting \033[0;32m$file \033[0;31m--->>\033[0;32m ${output_dir}${file//.bos/.sh}\033[0m"
     bos "$file" ${output_dir}"${file//.bos/.sh}"
     chmod +x ${output_dir}"${file//.bos/.sh}"
  fi
done
