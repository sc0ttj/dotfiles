#!/bin/bash

# Usage:
#
# Call the script in the directory containing the target files to convert.
# There are no arguments.

# Info:
#
# The script will copy both the audio and video bitrates into the new file
# via metadata from ffprobe and will retain the original DTS for you.

export BLD="\e[01m" RED="\e[01;31m" BLU="\e[01;34m" NRM="\e[00m"

command -v ffmpeg >/dev/null 2>&1 || {
echo -e "${BLD}${RED}I require ffmpeg but it's not installed. Aborting.${NRM}" >&2
exit 1; }

command -v ffprobe >/dev/null 2>&1 || {
echo "${BLD}${RED}I require ffprobe but it's not installed. Aborting.${NRM}" >&2
exit 1; }

report() {
  echo -e "${BLD}${RED}Task $n of $total${NRM}"
  echo -e "${BLD} file to encode    :${BLU} ${file%.*}"|sed 's|./||'
  echo -e "${NRM}${BLD} video bitrate     : $vkbps kbps${NRM}"
  echo -e "${BLD} audio bitrate     : $akbps kbps${NRM}"
  echo -e "${BLD} play time         : $ptime${NRM}"
}

pass1() {
  # For syntax, see: https://trac.ffmpeg.org/wiki/Encode/H.264

  nice -19 ffmpeg -v quiet -stats -y -i "${file}" \
    -c:v libx264 -preset slow -b:v "$vkbps"k -pass 1 \
    -passlogfile "$logfile" \
    -c:a aac -b:a "$akbps"k -f mp4 /dev/null
}

pass2() {
  nice -19 ffmpeg -v quiet -stats -i "${file}" -c:v libx264 \
    -preset slow -b:v "$vkbps"k -pass 2 \
    -passlogfile "$logfile" \
    -c:a aac -b:a "$akbps"k "${file%.*}.mp4"

  # remove the 1st pass log and mbtree files
  rm -f "$logfile"*
}

if df -T /tmp | grep -m 1 -q tmpfs; then
  # /tmp is tmpfs so use it for the mbtree and log files
  logfile=/tmp/auto-reencode_ffmpeg2pass
else
  # /tmp is not tmpfs so just writeout mbtree and logs in the working dir
  logfile=auto-reencode_ffmpeg2pass
fi

IFS=$'\012'
echo 'Enter target file extension [flv,wmv,asf]:'
read -r EXT

# check for files to work on and exit if not are found
if [[ -z $(find . ! -name . -prune -type f -name "*.$EXT" 2>/dev/null) ]]; then
  EXT="flv"
  if [[ -z $(find . ! -name . -prune -type f -name "*.$EXT" 2>/dev/null) ]]; then
    EXT="wmv"
    if [[ -z $(find . ! -name . -prune -type f -name "*.$EXT" 2>/dev/null) ]]; then
      EXT="asf"
      if [[ -z $(find . ! -name . -prune -type f -name "*.$EXT" 2>/dev/null) ]]; then
        echo -e "${BLD}Cannot find any target files so no work to do.${NRM}"
        echo -e "${BLD}Define a target file type in this dirtree and try again.${NRM}"
        exit 1
      fi
    fi
  fi
fi

[[ -z "$EXT" ]] && EXT="flv"

total=$(find . ! -name . -prune -type f -name "*.$EXT"|wc -l)

while IFS= read -r -d '' file
do
  (( n++ ))
  # find bitrate for video and audio
  vkbps=$(( $(ffprobe -v error -select_streams v:0 -show_entries stream=bit_rate -of default=noprint_wrappers=1:nokey=1 "${file}") / 1000))
  akbps=$(( $(ffprobe -v error -select_streams a:0 -show_entries stream=bit_rate -of default=noprint_wrappers=1:nokey=1 "${file}") / 1000))
  # ffprobe reports time in sec with a fuckton of decimals, piping through awk rounds to a int
  secs=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "$file"|uniq|awk '{print int($1+0.5)}')
  ptime=$(printf '%02dh:%02dm:%02ds\n' $(($secs/3600)) $(($secs%3600/60)) $(($secs%60)))

  report

  # do not overwrite existing files
  if [[ -e "${file%.*}.mp4" ]]; then
    echo >&2 Output file already exists: "${file%.*}.mp4"
    echo >&2 Skipping...
    echo >&2
    continue
  fi

  # encode
  echo -e "${BLD} pass 1 of 2 stats : ${NRM}"

  if pass1; then
    echo -e "${BLD} pass 2 of 2 stats : ${NRM}"
    pass2
  else
    echo -e "${BLD}${RED} Pass 1 failed, exiting!${NRM}"
    exit 1
  fi

  # restore dts
  touch -r "$file" "${file%.*}.mp4"
done <   <(find . ! -name . -prune -type f -name "*.$EXT" -print0)

# vim:set ts=2 sw=2 et:
