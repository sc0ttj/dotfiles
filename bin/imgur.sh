#!/bin/sh 
# imgur - post image to imgur.com

curl -F "image=@$1" -F key=b3625162d3418ac51a9ee805b1840452 \
  http://imgur.com/api/upload.xml | 
  sed -ne 's|.*<original_image>\(.*\)</original_image>.*|\1|p'
