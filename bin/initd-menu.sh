#!/bin/bash
#
# oetv.sh
# Show all servives in /etc/init.d in a list, and let the user choose how to start it.
#
# (c) 2008 Stefan Wagner, license GPLv3
#
# Search /etc/init.d/ for all executable files
# Get their number, and the maximum name size to produce a fitting window

width=0
height=0

# The font will influence the optimal window size
# But I don't know how to get them.
# Probably depending on windowmanager, desktop, usersettings

function xyFromList
{
    anz=0
    wmax=0
    for file in $1
    do
        anz=$((anz+1))
        len=${#file}
        [ $len -gt $wmax ] && wmax=$len
    done;
    width=$((wmax*9+50))
    height=$((anz*26+160))
}

dienstlist=$(ls /etc/init.d/ )
xyFromList "$dienstlist"

dienst=$(zenity --width=$width --height=$height --list --text "Service schalten" --column "Dienst" $dienstlist)
[ "foo"$dienst == "foo" ] && exit

# select options for the service, and display an apropriate window

optionen=$(egrep -h "[a-z]+\)" /etc/init.d/$dienst | sed 's/^[ \t]*//;s/).*/)/;s/#.*//;s/)//g;s/|/ /g' | sort -u)
xyFromList "$optionen"
aktion=$(zenity --width=$width --height=$height --list --text "Service schalten" --column "Befehl" $optionen)
[ "foo"$aktion == "foo" ] && exit
result=$(gksudo /etc/init.d/$dienst $aktion)

zenity --info "$aktion" --text "$result"
