#!/usr/bin/env bash

# Markdown to HTML (https://gist.github.com/jaburns/33d88f4aa99bb4ab6ad6)

# This script uses the GitHub API to do the conversion, so requires an
# internet connection.

# Usage examples:

#  md2html file.md              # print HTML to stdout
#  md2html file.md > file.html  # create HTML file

if [ ! -f "$1" ];then
  echo "Error: $1 is not a file!"
  exit 1
fi

# escape slashes and new lines in the given markdown
data="$(
    cat "$1" \
      | sed 's/"/\\"/g' \
      | sed ':a;N;$!ba;s/\n/\\n/g')"

context=''
[ ! -z "$2" ] && context=",\"context\":\"$2\""

# convert hmtl to markdown using Github API
curl -s --data "{\"text\":\"$data\",\"mode\":\"gfm\"$context}" 'https://api.github.com/markdown'

unset data
unset context
