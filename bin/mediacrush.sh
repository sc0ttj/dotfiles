#!/bin/sh
# mediacrush - post image to mediacru.sh

if [ "$1" = -t ]; then
  shift
  convert -resize 250x250 "$1" jpg:- |
  curl -F 'file=@-;filename="thumb.jpg"' https://mediacru.sh/api/upload/file |
  sed -ne '/^  "hash"/s,.*"\([^"]*\)"[^"]*$,https://mediacru.sh/\1.jpg,p'
else
  curl -F "file=@$1" https://mediacru.sh/api/upload/file | tee /dev/stderr |
  sed -ne '/^  "hash"/s,.*"\([^"]*\)"[^"]*$,https://mediacru.sh/\1'.${1##*.}',p'
fi
