#!/bin/bash

# A shell function for `mo` - it adds support for mustache partials,
# with support for passing paramters to `mo`, for custom output.


# TDLR: this function replaces
#
#    {{>filename param1=foo param2=bar}}
#
# with
#
#    param1=foo param2=bar ${PARTIAL_DIR:-$PWD}/templates/_foo.mustache | mo
#
# It should be used inside `mo`, just before it process as a file.


# If $PARTIAL_DIR is not set, the current working directory ($PWD) is where
# the function will look for partial files.


# Usage:
#
#    process_partial {{>page-header title="bar" desc="one" three="3"}}


process_partial() {
  local partial="$(echo "$@" | cut -f1 -d ' '   | sed 's/{{>//')"

  if [ ! -f ${PARTIAL_DIR:-$PWD}/templates/_${partial}.mustache ];then
    echo "Error: partial file '${partial}.mustache' not found in: "
    echo "${PARTIAL_DIR:-$PWD}/templates"
    echo
    return 1
  fi

  local params="$(echo  "$@" | cut -f2-99 -d' ' | sed "s/}}$//")"

  source `which mo`

  ${params} cat ${PARTIAL_DIR:-$PWD}/templates/_${partial}.mustache | mo
}


