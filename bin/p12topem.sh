#!/bin/sh

if [ ! "$1" ] || [ ! "$2" ];then
    echo 'p12topem.sh - Convert .p12 certificate files to .pem format'
    echo
    echo 'Usage:'
    echo
    echo '    p12topem.sh /path/to/somefile.p12 /path/to/mynewfile.pem'
    exit 0
fi

openssl pkcs12 -in "$1" -out "$2" -clcerts -nodes
exit 0
