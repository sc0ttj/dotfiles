#!/bin/sh

#tr "'" '_' | \

for i in * ;
    do
        mv -nv "$i" `echo "$i" | \
          iconv -f utf8 -t ascii//TRANSLIT | \
          tr [:upper:] [:lower:] | \
          tr ' ' '_' | \
          tr '-' '_' | \
          tr ',' '_' | \
          tr ':' '_' | \
          tr '#' '_' | \
          tr ';' '_' | \
          tr '~' '_' | \
          tr "?" '_' | \
          sed "s/'//g" | \
          sed "s/\._/_/g" | \
          sed "s/_\././g" | \
          sed "s/^_//g" | \
          tr -s '_' '_' `;
    done
