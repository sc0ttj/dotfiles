#!/bin/bash


if [ -z "$1" ] || [ -z "$2" ];then
  echo "Error: I need a magnet link and a server to send it to!"
  echo "$0 <magnet link> <host or local ip>"
  echo
  echo "Example:"
  echo
  echo "$0 \"magnet:?xt=...EE1A\" 192.168.1.5"
  exit 1
fi
 
HOST="${2}"
PORT=9092
USER=root
PASS=woofwoof
 
LINK="$1"

# set true if you want every torrent to be paused initially
#PAUSED="true"
PAUSED="false"

SESSID=$(curl --silent --anyauth --insecure --user $USER:$PASS "https://$HOST:$PORT/transmission/rpc" | sed 's/.*<code>//g;s/<\/code>.*//g')

xmessage "SESSID='$SESSID'"

[ "$SESSID" = "" ] && exit 1

curl --silent --anyauth --insecure --user $USER:$PASS --header "$SESSID" "https://$HOST:$PORT/transmission/rpc" -d "{\"method\":\"torrent-add\",\"arguments\":{\"paused\":${PAUSED},\"filename\":\"${LINK}\"}}" || { echo "curl failed"; exit 1; }

exit 0
