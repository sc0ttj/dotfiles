#!/usr/bin/perl -00
# unfmt - convert paragraphs into long lines

while(<>) {
  chomp;
  s/(?<=\S)\r?\n(?=\S)/ /g;
  s/\r?\n//g;
  print "$_\n";
}
