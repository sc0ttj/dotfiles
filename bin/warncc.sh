#!/bin/sh
# warncc - gcc/clang wrapper with many warnings

case "$WARNCC" in
clang)
	# clang 4.0.1
	exec $WARNCC -Weverything \
		-Wno-sign-conversion -Wno-missing-prototypes -Wno-switch \
		-Wno-missing-variable-declarations -Wno-disabled-macro-expansion \
		-Wformat=2 \
		-fstack-protector-strong -O2 -D_FORTIFY_SOURCE=2 "$@"
	;;
gcc|cc|'')
	# gcc 7.2
	exec ${WARNCC:-gcc} -Wall -Wno-switch -Wextra \
		-Wduplicated-cond -Wduplicated-branches -Wrestrict \
		-Wlogical-op -Wnull-dereference \
		-Wjump-misses-init -Wdouble-promotion -Wformat=2 \
		-fstack-protector-strong -O2 -D_FORTIFY_SOURCE=2 "$@"
	;;
esac
