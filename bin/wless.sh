#!/bin/sh
# wless - web page less
export LESSOPEN='|/usr/bin/lesspipe.sh %s'
TEMPFILE=$(mktemp)
trap "rm -f $TEMPFILE" INT TERM EXIT
curl -L "$@" -o $TEMPFILE
less $TEMPFILE
